# This is a Boost library, so we need Boost's header files.
find_package(Boost REQUIRED)
include_directories(${Boost_INCLUDE_DIR})

# Create a library with Boost's signals source files.
add_library(signals STATIC
    connection.cpp
    named_slot_map.cpp
    signal_base.cpp
    slot.cpp
    trackable.cpp
    )
