get_filename_component(_benzaiten_cmake_path ${CMAKE_CURRENT_LIST_FILE} PATH)
set(_benzaiten_wix_path ${_benzaiten_cmake_path}/../wix CACHE INTERNAL "")

##
# Adds a MSI database.
#
macro(ADD_BENZAITEN_MSI _name _postfix)
    # This only works for Windows.
    if(WIN32)
        find_program(WIX_CANDLE candle)
        find_program(WIX_LIGHT light)
        if(WIX_CANDLE AND WIX_LIGHT)
            set(_wix_source_file ${CMAKE_CURRENT_BINARY_DIR}/${_name}.wxs)
            set(_wix_obj_file ${_name}.wixobj)
            set(_msi_file ${_name}${_postfix}.msi)
            set(_exit_dialog_file ${_benzaiten_wix_path}/ExitDialog.wxs)
            set(_installdir_file ${_benzaiten_wix_path}/Benzaiten_InstallDir.wxs)

            configure_file(${_name}.wxs.cmake ${_wix_source_file} @ONLY)
            add_custom_target(msi_${_name}
                ${WIX_CANDLE} -nologo -out ExitDialog.wixobj ${_exit_dialog_file}
                COMMAND ${WIX_CANDLE} -nologo -out Benzaiten_InstallDir.wixobj ${_installdir_file}
                COMMAND ${WIX_CANDLE} -nologo -out ${_wix_obj_file} -ext WixUtilExtension -ext WixGamingExtension ${_wix_source_file}
                COMMAND ${WIX_LIGHT} -nologo -out ${_msi_file} -dWixUILicenseRtf=${CMAKE_SOURCE_DIR}/COPYING.rtf -ext WixUtilExtension -ext WixUIExtension -ext WixGamingExtension -cultures:en-us ${_wix_obj_file} ExitDialog.wixobj Benzaiten_InstallDir.wixobj
                DEPENDS ${_name}
                WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                COMMENT "Generating MSI database ${_msi_file}")
        else(WIX_CANDLE AND WIX_LIGHT)
            message(STATUS "candle and/or light couldn't be found.  Skipping MSI target")
        endif(WIX_CANDLE AND WIX_LIGHT)
    endif(WIN32)
endmacro(ADD_BENZAITEN_MSI)
