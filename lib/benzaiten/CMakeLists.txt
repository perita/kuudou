# I've only tested this project with CMake 2.6.
# So for now this is the required version.
cmake_minimum_required(VERSION 3.0)

# The project's name.  This will be the name of the solution file
# for Microsoft Visual Studio, for example.
project(Benzaiten)

# Remove some warnings in MSVC.
if(MSVC)
    add_definitions(-D_SCL_SECURE_NO_WARNINGS)
endif(MSVC)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/lib)
add_definitions(-DBOOST_SIGNALS_NO_DEPRECATION_WARNING)
add_definitions(-DBOOST_BIND_GLOBAL_PLACEHOLDERS)

# Process subdirectories.
add_subdirectory(3rdparty)
add_subdirectory(doc)
add_subdirectory(lib)
add_subdirectory(src)
