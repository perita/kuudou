//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_CONFIG_H
#include "Surface.hpp"
#include <cassert>
#include <new>
#include <stdexcept>
#include <boost/function.hpp>
#include <boost/lambda/lambda.hpp>

#if defined (EMSCRIPTEN)
// There is no SDL_DisplayFormat in Emscripten
#define SDL_DisplayFormat SDL_DisplayFormatAlpha
#endif // EMSCRIPTEN

using namespace benzaiten;

Surface::Surface (size_t width, size_t height, unsigned int bitsPerPixel):
surface_ (SDL_CreateRGBSurface (SDL_SWSURFACE,
            width, height, bitsPerPixel, 0, 0, 0, 0),
            SDL_FreeSurface)
{
    if ( 0 == surface_.get () )
    {
        throw std::bad_alloc ();
    }

    SDL_SetColorKey (surface_.get (), SDL_SRCCOLORKEY | SDL_RLEACCEL,
            mapRGBA (255, 0, 255));

    if ( 32 == bitsPerPixel )
    {
        surface_ = boost::shared_ptr<SDL_Surface> (
            SDL_DisplayFormatAlpha (surface_.get()), SDL_FreeSurface);
    }
    else
    {
        surface_ = boost::shared_ptr<SDL_Surface> (
            SDL_DisplayFormat (surface_.get()), SDL_FreeSurface);
    }

    if ( 0 == surface_.get () )
    {
        throw std::bad_alloc ();
    }
}

Surface::Surface (size_t width, size_t height, const Surface &format):
surface_ (SDL_CreateRGBSurface (SDL_SWSURFACE,
            width, height, format.bitsPerPixel (), 0, 0, 0, 0),
            SDL_FreeSurface)
{
    if ( 0 == surface_.get () )
    {
        throw std::bad_alloc ();
    }

    surface_ = boost::shared_ptr<SDL_Surface> (
            SDL_ConvertSurface (surface_.get(), format.surface ()->format,
                format.surface ()->flags), SDL_FreeSurface);

    if ( 0 == surface_.get () )
    {
        throw std::bad_alloc ();
    }

    SDL_SetColorKey (surface_.get (), SDL_SRCCOLORKEY | SDL_RLEACCEL,
            mapRGBA (255, 0, 255));
}

Surface::Surface (boost::shared_ptr<SDL_Surface> surface):
    surface_ (surface)
{
    assert ( surface && "Tried to set a non valid surface pointer" );
}

unsigned int
Surface::bitsPerPixel () const
{
    return surface ()->format->BitsPerPixel;
}

SDL_Rect
Surface::blit (Surface &destination) const
{
    return blit (0, 0, destination);
}

SDL_Rect
Surface::blit (int16_t x, int16_t y, Surface &destination) const
{
    return blit (0, 0, width (), height (), x, y, destination);
}

SDL_Rect
Surface::blit (int16_t sourceX, int16_t sourceY, uint16_t width,
        uint16_t height, int16_t destX, int16_t destY,
        Surface &destination) const
{
    SDL_Rect sourceRect = {sourceX, sourceY, width, height};
    return blit (sourceRect, destX, destY, destination);
}

SDL_Rect
Surface::blit (SDL_Rect &source, int16_t destX, int16_t destY,
        Surface &destination) const
{
    SDL_Rect destRect = {destX, destY, source.w, source.h};
    // I think this is safe, as blitting does not affect the source
    // surface, but SDL_BlitSurface uses a non-const surface.
    SDL_Surface *sourceSurface = const_cast<Surface &> (*this).surface ();
    SDL_BlitSurface (sourceSurface, &source, destination.surface (), &destRect);
    return destRect;
}

Surface
Surface::copy () const
{
    // I assume this is safe, since we don't change the surface.
    SDL_Surface *source = const_cast<Surface &> (*this).surface ();
    boost::shared_ptr<SDL_Surface> copy (
            SDL_ConvertSurface (source, source->format, source->flags),
            &SDL_FreeSurface);
    if ( 0 == copy.get () )
    {
        throw std::bad_alloc ();
    }
    return Surface (copy);
}

uint32_t
Surface::mapRGBA (uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha) const
{
    return SDL_MapRGBA (surface ()->format, red, green, blue, alpha);
}

void
Surface::fill (const Color &color)
{
    fill (color.red, color.green, color.blue, color.alpha);
}

void
Surface::fill (uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha)
{
    fill (red, green, blue, alpha, 0, 0, width (), height ());
}

void
Surface::fill (const Color &color, int16_t x, int16_t y, uint16_t width,
        uint16_t height)
{
    fill (color.red, color.green, color.blue, color.alpha, x, y, width, height);
}

void
Surface::fill (uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha,
        int16_t x, int16_t y, uint16_t width, uint16_t height)
{
    SDL_Rect rect = {x, y, width, height};
    SDL_FillRect (surface (), &rect, mapRGBA (red, green, blue, alpha));
}

void
Surface::flip ()
{
    SDL_Flip (surface ());
}

uint32_t
Surface::getPixel (int16_t x, int16_t y) const
{
    assert ( x > -1 && x < width () );
    assert ( y > -1 && y < height () );


    uint32_t pixelColor = 0;
    SDL_Surface *surface = const_cast<Surface &> (*this).surface ();
    ScopedLock lock (surface);
    switch ( surface->format->BytesPerPixel )
    {
        case 1:
            pixelColor = static_cast<uint32_t>(
                    *(static_cast<uint8_t *> (surface->pixels) + y * surface->pitch + x));
            break;

        case 2:
            pixelColor = static_cast<uint32_t> (
                    *(static_cast<uint16_t *> (surface->pixels) + y * surface->pitch / 2 + x));
            break;

        case 3:
            {
                uint8_t *pixels = static_cast<uint8_t *> (surface->pixels) +
                    y * surface->pitch + x * 3;
                uint8_t red = *(pixels + surface->format->Rshift / 8);
                uint8_t green = *(pixels + surface->format->Gshift / 8);
                uint8_t blue = *(pixels + surface->format->Bshift / 8);
                pixelColor = mapRGBA (red, green, blue, 255);
            }
            break;

        case 4:
            pixelColor = *(static_cast<uint32_t *> (surface->pixels) +
                    y * surface->pitch / 4 + x);
            break;
    }

    return pixelColor;
}

int
Surface::height () const
{
    return surface ()->h;
}

void
Surface::putPixel (int16_t x, int16_t y, uint32_t pixel)
{
    assert ( x > -1 && x < width () );
    assert ( y > -1 && y < height () );

    SDL_Surface *surface = this->surface ();
    ScopedLock lock (surface);
    switch ( surface->format->BytesPerPixel )
    {
        case 1:
            *(static_cast<uint8_t *> (surface->pixels) + y * surface->pitch + x) = pixel;
            break;

        case 2:
            *(static_cast<uint16_t *> (surface->pixels) + y * surface->pitch / 2 + x) = pixel;
            break;

        case 3:
            {
                uint8_t red = 0;
                uint8_t green = 0;
                uint8_t blue = 0;
                uint8_t alpha = 0;
                unmapRGBA (pixel, red, green, blue, alpha);

                uint8_t *pixels = static_cast<uint8_t *> (surface->pixels) +
                    y * surface->pitch + x * 3;
                *(pixels + surface->format->Rshift / 8) = red;
                *(pixels + surface->format->Gshift / 8) = green;
                *(pixels + surface->format->Bshift / 8) = blue;
            }
            break;

        case 4:
            *(static_cast<uint32_t *> (surface->pixels) +
                    y * surface->pitch / 4 + x) = pixel;
            break;
    }
}

Surface
Surface::screen ()
{
    SDL_Surface *screen = SDL_GetVideoSurface ();
    assert ( 0 != screen && "There is no screen surface!" );
    boost::shared_ptr<SDL_Surface> copy (
            SDL_CreateRGBSurface (SDL_SWSURFACE | SDL_SRCALPHA,
                screen->w, screen->h, screen->format->BitsPerPixel,
                screen->format->Rmask, screen->format->Gmask,
                screen->format->Bmask, screen->format->Amask),
            SDL_FreeSurface);
    if ( !copy )
    {
        throw std::runtime_error (SDL_GetError ());
    }
    SDL_BlitSurface (screen, 0, copy.get (), 0);

    return Surface (copy);
}

void
Surface::setAlpha (uint8_t alpha)
{
    SDL_SetAlpha (surface (), SDL_SRCALPHA, alpha);
}

const SDL_Surface *
Surface::surface () const
{
    assert ( surface_ && "There is no SDL_Surface here" );
    return surface_.get ();
}

SDL_Surface *
Surface::surface ()
{
    assert ( surface_ && "There is no SDL_Surface here" );
    return surface_.get ();
}

void
Surface::unmapRGBA (uint32_t pixel, uint8_t &red, uint8_t &green,
        uint8_t &blue, uint8_t &alpha) const
{
    SDL_GetRGBA (pixel, surface ()->format, &red, &green, &blue, &alpha);
}

int
Surface::width () const
{
    return surface ()->w;
}

namespace
{
    void
    NoOp (SDL_Surface *)
    {
    }
}

namespace benzaiten
{
    SDL_Surface *
    fastScale (const SDL_Surface *surface, uint16_t ratio)
    {
        assert ( ratio > 0 && "Invalid scale ratio" );
        // A scaling ratio of 1 in an special case that I just return a copy
        // since it will be faster than getting and putting pixels.
        if ( 1 == ratio )
        {
            SDL_Surface *copy =
                SDL_ConvertSurface (const_cast<SDL_Surface *> (surface),
                        surface->format, surface->flags);
            if ( 0 == copy )
            {
                throw std::bad_alloc ();
            }
            return copy;
        }

        const int surfaceHeight = surface->h;
        const int scaledHeight = surfaceHeight * ratio;
        const int surfaceWidth = surface->w;
        const int scaledWidth = surfaceWidth * ratio;

        SDL_Surface *scaled =
            SDL_CreateRGBSurface (SDL_SWSURFACE, scaledWidth, scaledHeight,
                    surface->format->BitsPerPixel, 0, 0, 0, 0);
        if ( 0 == scaled )
        {
                throw std::bad_alloc ();
        }

        {
            SDL_Surface *scaledFormat =
                SDL_ConvertSurface (scaled, surface->format, surface->flags);
            SDL_FreeSurface (scaled);
            scaled = scaledFormat;
        }

        if ( 0 == scaled )
        {
                throw std::bad_alloc ();
        }
        SDL_SetColorKey (scaled, SDL_SRCCOLORKEY | SDL_RLEACCEL,
                SDL_MapRGBA (scaled->format, 255, 0, 255, 255));

        Surface source (boost::shared_ptr<SDL_Surface> (
                    const_cast<SDL_Surface *> (surface), &NoOp));
        Surface dest (boost::shared_ptr<SDL_Surface> (scaled, &NoOp));

        for (int y = 0 ; y < surfaceHeight ; ++y )
        {
            int scaledYBegin = y * ratio;
            int scaledYEnd = scaledYBegin + ratio;
            for ( int x = 0 ; x < surfaceWidth ; ++x )
            {
                uint32_t pixel = source.getPixel (x, y);

                int scaledXBegin = x * ratio;
                int scaledXEnd = scaledXBegin + ratio;
                for ( int scaledY = scaledYBegin ; scaledY < scaledYEnd ;
                        ++scaledY )
                {
                    for ( int scaledX = scaledXBegin ; scaledX < scaledXEnd ;
                            ++scaledX )
                    {
                        dest.putPixel (scaledX, scaledY, pixel);
                    }
                }
            }
        }

        return scaled;
    }

    Surface flip (const Surface &surface, bool flip_x, bool flip_y) {
        using boost::lambda::_1;

        Surface flipped (surface.width (), surface.height (), surface);

        typedef boost::function<int16_t(int16_t)> CoordinateMap;
        CoordinateMap map_x;
        if (flip_x) {
            map_x = surface.width () - 1 - _1;
        } else {
            map_x = _1;
        }
        CoordinateMap map_y;
        if (flip_y) {
            map_y = surface.height () - 1 - _1;
        } else {
            map_y = _1;
        }

        for (int y = 0 ; y < surface.height () ; ++y ) {
            for (int x = 0 ; x < surface.width () ; ++x) {
                flipped.putPixel (map_x (x), map_y (y), surface.getPixel (x, y));
            }
        }

        return flipped;
    }
}
