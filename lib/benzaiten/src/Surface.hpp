//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SURFACE_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SURFACE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#if !defined(_MSC_VER)
#include <stdint.h>
#endif // !_MSC_VER
#include <SDL.h>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

namespace benzaiten
{
    ///
    /// @class Surface
    /// @brief Graphical surface class.
    ///
    class Surface
    {
        public:
            /// An smart pointer to a surface.
            typedef boost::shared_ptr<Surface> ptr;

            ///
            /// @enum Flags
            /// @brief Surface creation flags.
            ///
            enum Flags
            {
                /// Create the video surface in video memory.
                Hardware = SDL_HWSURFACE,
                /// Create the video surface in system memory.
                Software = SDL_SWSURFACE
            };

            ///
            /// @class Color
            /// @brief A surface's color.
            ///
            struct Color
            {
                /// The alpha component of the color.
                uint8_t alpha;
                /// The blue compoment of the color.
                uint8_t blue;
                /// The green component of the color.
                uint8_t green;
                /// The red component of the color.
                uint8_t red;

                ///
                /// @brief Constructor.
                ///
                /// @param[in] red The red component of the color.
                /// @param[in] green The green component of the color.
                /// @param[in] blue The blue component of the color.
                /// @param[in] alpha The alpha component of the color.
                ///
                Color (uint8_t red, uint8_t green, uint8_t blue,
                        uint8_t alpha =  255):
                    alpha (alpha),
                    blue (blue),
                    green (green),
                    red (red)
                {
                }
            };

            ///
            /// @brief Create an empty Surface.
            ///
            /// @param[in] width The width of the Surface to create.
            /// @param[in] height The height of the Surface to create.
            /// @param[in] bitsPerPixel the bits per pixel of the Surface.
            ///
            Surface (size_t width, size_t height, unsigned int bitsPerPixel);

            ///
            /// @brief Create an empty Surface.
            ///
            /// @param[in] width The width of the Surface to create.
            /// @param[in] height The height of the Surface to create.
            /// @param[in] format The surface to use its format to create
            ///            the new surface.
            ///
            Surface (size_t width, size_t height, const Surface &format);

            ///
            /// @brief Initializes a Surface with a pointer to an SDL_Surface.
            ///
            /// @param[in] surface The smart pointer to the SDL_Surface to
            ///            hold.  The deleter must be set correctly to point
            ///            to SDL_FreeSurface().
            ///
            Surface (boost::shared_ptr<SDL_Surface> surface);

            ///
            /// @brief Gets the bits per pixel of the surface.
            ///
            /// @return The surface's bits per pixel.
            ///
            unsigned int bitsPerPixel () const;

            ///
            /// @brief Performs a fast blit of the surface to the destination.
            ///
            /// Performs a fast blit from the whole surface to the top right
            /// position of @p destination.
            ///
            /// @param[in] destination The destination surface where to blit
            ///            this surface.
            ///
            /// @return The rectangle where the surface has been blit to.
            ///
            SDL_Rect blit (Surface &destination) const;

            ///
            /// @brief Perfoms a fast blit of the surface to the destination.
            ///
            /// Performs a fast blit of the whole surface to the specified
            /// position on the destination surface.
            ///
            /// @param[in] x The X position to blit the surface to.
            /// @param[in] y The Y position to blit the surface to.
            /// @param[in] destination The destination surface where to blit
            ///            this surface.
            ///
            /// @return The rectangle where the surface has been blit to.
            ///
            SDL_Rect blit (int16_t x, int16_t y, Surface &destination) const;

            ///
            /// @brief Performs a fast blit of the surface to the destination.
            ///
            /// Perform a fast blit of the specified portion of the surface
            /// to the specified position on the destination surface.
            ///
            /// @param[in] sourceX The X position of the surface to start
            ///            the blit from.
            /// @param[in] sourceY The Y position of the surface to start
            ///            the blit from,
            /// @param[in] width The width of the surface's part to blit.
            /// @param[in] height The height of the surface's part to blit.
            /// @param[in] destX The X position to blit to @p destination.
            /// @param[in] destY The Y position to blit to @p destination.
            /// @param[in] destination The destination surface where to blit
            ///            this surface.
            ///
            /// @return The rectangle where the surface has been blit to.
            ///
            SDL_Rect blit (int16_t sourceX, int16_t sourceY, uint16_t width,
                    uint16_t height, int16_t destX, int16_t destY,
                    Surface &destination) const;

            ///
            /// @brief Performs a fast blit of the surface to the destination.
            ///
            /// Perfoms a fast blit of the specified portion of the surface
            /// to the specified position on the destination surface.
            ///
            /// @param[in] source The rectangle with the position and size
            ///            of the portion of the surface blit from.
            /// @param[in] destX The X position to blit to @p destination.
            /// @param[in] destY The Y position to blit to @p destination.
            /// @param[in] destination The destination surface where to blit
            ///            this surface.
            ///
            /// @return The rectangle where the surface has been blit to.
            ///
            SDL_Rect blit (SDL_Rect &source, int16_t destX, int16_t destY,
                    Surface &destination) const;

            ///
            /// @brief Creates a new surface that is a copy.
            ///
            /// @return A new surface that is a pixel by pixel copy of this
            ///         surface.  The new surface's format is exactly the
            ///         same.
            ///
            Surface copy () const;

            ///
            /// @brief Fast fills the whole surface with the given color.
            ///
            /// @param color The color to fill with.
            ///
            void fill (const Color &color);

            ///
            /// @brief Fast fills the whole surface with the given color.
            ///
            /// @param[in] red The red component of the color to fill with.
            /// @param[in] green The green component of the color to fill with.
            /// @param[in] blue The blue component of the color to fill with.
            /// @param[in] alpha The alpha component of the color to fill with.
            ///
            void fill (uint8_t red, uint8_t green, uint8_t blue,
                    uint8_t alpha = 255);

            ///
            /// @brief Fast fills a surface's part with the given color.
            ///
            /// @param[in] color The color to fill with.
            /// @param[in] x The starting horizontal position of the surface's
            ///            part to fill.
            /// @param[in] y The starting vertical position of the surface's
            ///            part to fill.
            /// @param[in] width The width of the surface's part to fill.
            /// @param[in] height The height of the surface's part to fill.
            ///
            void fill (const Color &color, int16_t x, int16_t y, uint16_t width,
                    uint16_t height);

            ///
            /// @brief Fast fills a surface's part with the given color.
            ///
            /// @param[in] red The red component of the color to fill with.
            /// @param[in] green The green component of the color to fill with.
            /// @param[in] blue The blue component of the color to fill with.
            /// @param[in] alpha The alpha component of the color to fill with.
            /// @param[in] x The starting horizontal position of the surface's
            ///            part to fill.
            /// @param[in] y The starting vertical position of the surface's
            ///            part to fill.
            /// @param[in] width The width of the surface's part to fill.
            /// @param[in] height The height of the surface's part to fill.
            ///
            void fill (uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha,
                    int16_t x, int16_t y, uint16_t width, uint16_t height);

            ///
            /// @brief Swaps screen buffers.
            ///
            /// On hardware that supports double-buffering, this function
            /// sets up a flip and returns.  The hardware will wait retrace,
            /// and then swap video buffers before the next video blit or lock
            /// will return.  On hardware that does not support
            /// double-buffering of if Surface::Software was set, this function
            /// makes sure that the updates in the surface are visible on
            /// the screen.
            ///
            /// A software screen surface is also updated automatically when
            /// parts of an SDL window are redrawn, caused by overlapping
            /// windows or by restoring from an iconified state.  As a result
            /// there is no proper double buffer behaviour in windowed mode
            /// for a software screen, in contrast to a full screen software
            /// mode.
            ///
            void flip ();

            ///
            /// @brief Gets a single pixel from the surface.
            ///
            /// @param[in] x The X coordinate of the pixel to get.
            /// @param[in] y The Y coordinate of the pixel to get.
            /// @return The value of the pixel at @p x and @p y.
            ///
            uint32_t getPixel (int16_t x, int16_t y) const;

            ///
            /// @brief Gets the surface's height.
            ///
            /// @return The surface's height.
            ///
            int height () const;

            ///
            /// @brief Maps a RGBA color value to a pixel format.
            ///
            /// @param[in] red The red component to map.
            /// @param[in] green The green component to map.
            /// @param[in] blue The blue component to map.
            /// @param[in] alpha The alpha component to map.  If the Surface
            ///            has no alpha component this value will be ignored.
            ///
            /// @return A pixel value bes approximating the given @p red,
            ///         @p green, and @p blue color for the Surface.  If
            ///         the color depth is less than 32, then the unused
            ///         upper bits of the return can safely be ignored (e.g.,
            ///         with a 16 bits per pixel surface the return value can
            ///         be assigned to an uint16_t, and similarly a uint8_t for
            ///         a 8 bits per pixel Surface.)
            ///
            uint32_t mapRGBA (uint8_t red, uint8_t green, uint8_t blue,
                    uint8_t alpha = 255) const;

            ///
            /// @brief Puts a single pixel to the surface.
            ///
            /// @param[in] x The X coordinate to put the pixel to.
            /// @param[in] y The Y coordinate to put the pixel to.
            /// @param[in] pixel The pixel to up at @p x and @p y.
            ///
            void putPixel (int16_t x, int16_t y, uint32_t pixel);

            ///
            /// @brief Creates a new surface with the contents of the screen.
            ///
            /// @return A new surface with the contents of the screen copied.
            ///
            /// @throw std::rutime_error If there is any problem creating the
            ///        surface.
            ///
            static Surface screen ();

            ///
            /// @brief Set the surface's alpha value.
            ///
            /// @param[in] alpha The alpha value to set to the surface.
            ///
            void setAlpha (uint8_t alpha);

            ///
            /// @brief Gets the RGBA component values from a pixel.
            ///
            /// This function uses the entire 8-bit range when converting
            /// color components from pixel formats with less than 8-bits
            /// per RGB components (e.g., a completely white pixel in 16-bit
            /// RGB565 format would return @c 0xff, @c 0xff, @c 0xff; not
            /// @c 0xf8, @c 0xfc, @c 0xf8.
            ///
            /// If the surface has no alpha component, @p alpha will be
            /// returned as 100% opaque (i.e., @c 0xff.)
            ///
            /// @param[in] pixel The pixel to get its RGBA component values.
            /// @param[out] red The reference where to store the red component
            ///             value.
            /// @param[out] green The reference where to store the green
            ///             component value.
            /// @param[out] blue The reference where to store the blue component
            ///             value.
            /// @param[out] alpha The reference where to store the alpha
            ///             component value.
            ///
            void unmapRGBA (uint32_t pixel, uint8_t &red, uint8_t &green,
                    uint8_t &blue, uint8_t &alpha) const;

            ///
            /// @brief Gets the surface's width.
            ///
            /// @return The surface's width.
            ///
            int width () const;

        private:
            friend class System;

            ///
            /// @class ScopedLock
            /// @brief Locks the access to a surface's pixels.
            ///
            class ScopedLock: public boost::noncopyable
            {
                public:
                    ScopedLock (SDL_Surface *surface):
                        surface_ (surface)
                    {
                        if ( SDL_MUSTLOCK (surface_) )
                        {
                            SDL_LockSurface (surface_);
                        }
                    }

                    ~ScopedLock ()
                    {
                        if ( SDL_MUSTLOCK (surface_) )
                        {
                            SDL_UnlockSurface (surface_);
                        }
                    }

                private:
                    SDL_Surface *surface_;
            };

            ///
            /// @brief Gets the constant pointer to the SDL_Surface.
            ///
            /// @return The constant pointer to the SDL_Surface.
            ///
            const SDL_Surface *surface () const;

            ///
            /// @brief Gets the pointer to the SDL_Surface.
            ///
            /// @return The pointer to the SDL_Surface.
            ///
            SDL_Surface *surface ();

            /// The pointer to the actual SDL surface.
            boost::shared_ptr<SDL_Surface> surface_;
    };

    ///
    /// @brief Fast scales a surface.
    ///
    /// This function scales a surface by duplicating each pixel
    /// without taking into account the neighbour pixel's information.
    /// It is only possible to scale up a surface, not down.
    ///
    /// @param[in] surface The surface to scale.
    /// @param[in] ratio The scale ration.  If it is 2, then for each pixel
    ///            in @p surface this function will duplicate it by 2 in X
    ///            and in Y.  0 is not a valid ratio.
    ///
    /// @return A new surface with @p surface scaled using @p ratio.
    ///
    SDL_Surface *fastScale (const SDL_Surface *surface, uint16_t ratio);

    ///
    /// @brief Flips a surface either vertically or horizontally.
    ///
    /// @param[in] surface The surface to flip.
    /// @paran[in] flip_x Whether to flip horizontally.
    /// @param[in] flip_y Whether to flip vertically.
    ///
    /// @return A new surface flipped vertically or horizontally or both or none.
    ///
    Surface flip (const Surface &surface, bool flip_x, bool flip_y);
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SURFACE_HPP
