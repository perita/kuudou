//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "FadeState.hpp"
#include "GameStateManager.hpp"

using namespace benzaiten;

FadeState::FadeState (float duration, uint8_t red, uint8_t green, uint8_t blue):
    alpha_ (0),
    background_ (),
    blue_ (blue),
    duration_ (duration),
    green_ (green),
    red_ (red)
{
}

int16_t
FadeState::alpha () const
{
    return alpha_;
}

void
FadeState::alpha (int16_t value)
{
    alpha_ = value;
}

void
FadeState::draw (Surface &screen)
{
    screen.fill (red_, green_, blue_);
    background_->blit (screen);
}

float
FadeState::duration () const
{
    return duration_;
}

void
FadeState::onActivate ()
{
    if ( !background_ )
    {
        background_ = background ();
    }
}

void
FadeState::update (float elapsedTime)
{
    updateAlpha (elapsedTime);
    if ( done () )
    {
        stateManager ().removeActiveState (Fade::None);
    }
    else
    {
        background_->setAlpha (alpha ());
    }
}
