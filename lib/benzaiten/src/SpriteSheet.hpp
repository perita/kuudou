//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SPRITE_SHEET_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SPRITE_SHEET_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include "Surface.hpp"

namespace benzaiten
{
    ///
    /// @class SpriteSheet
    /// @brief Cuts a Surface in different subviews.
    ///
    class SpriteSheet
    {
        public:
            ///
            /// @brief Initializes an SpriteSheet with a Surface.
            ///
            /// @param[in] surface The pointer to the Surface to use
            ///            as sheet.
            /// @param[in] width The width of each of the elements in
            ///            the sheet.  Must be greater than 0.
            /// @param[in] height The height of each of the elements in
            ///            the sheet. If <= 0 then the same value as \p width
            ///            is used.
            ///
            SpriteSheet (const Surface &surface, int width,
                    int height = -1);

            ///
            /// @brief Gets the number of sprites in the sheet.
            ///
            /// @return The number of sprites in the sheet.
            ///
            unsigned int count () const;

            ///
            /// @brief Draws an sprite from the sheet.
            ///
            /// @param[in] x The X position to draw the sprite.
            /// @param[in] y The Y position to draw the sprite.
            /// @param[in] index The sprite index to draw.  It must be less
            ///            than count().
            /// @param[in] screen The surface to draw the sprite to.
            ///
            /// @return The rectangle where the sprite has been drawn to.
            ///
            SDL_Rect draw (int16_t x, int16_t y, unsigned int index,
                    Surface &screen) const;

            ///
            /// @brief Gets the height of all sprites in the sheet.
            ///
            /// @return The height of each individual sprite.
            ///
            size_t height () const;

            ///
            /// @brief Sets the sheet's alpha value.
            ///
            /// @param[in] alpha The alpha value to set to the sprite sheet.
            ///
            void setAlpha (uint8_t alpha);

            ///
            /// @breief Gets the width of all sprites in the sheet.
            ///
            /// @return The width of each individual sprite.
            ///
            size_t width () const;

        private:
            /// The individial sprites rectangles.
            mutable std::vector<SDL_Rect> sprites_;
            /// The surface where the sprites are.
            mutable Surface surface_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SPRITE_SHEET_HPP
