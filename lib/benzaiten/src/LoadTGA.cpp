//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "LoadTGA.hpp"
#include <fstream>
#include <new>
#include <stdexcept>
#include <boost/function.hpp>


namespace
{
    ///
    /// @brief Checks if the architecture is big endian.
    ///
    /// @return @c true if the architecture is big endian, @c false if
    ///         is little endian.
    ///
    bool
    isBigEndian ()
    {
        uint8_t byteTest[2] = {1, 0};
        return 0 == (*(reinterpret_cast<uint16_t *> (byteTest)));
    }

    ///
    /// @brief Does nothing with a value.
    ///
    /// This function is here just to use in little endian architectures
    /// when converting little endiand shorts.
    ///
    /// @param[in] value The value to do nothing with.
    ///
    /// @return @p value.
    ///
    uint16_t
    noSwap (uint16_t value)
    {
        return value;
    }
}

namespace benzaiten
{
    struct TGAHeader
    {
        uint8_t idLength;
        uint8_t colorMapType;
        uint8_t imageType;
        uint16_t colorMapIndex;
        uint16_t colorMapLength;
        uint8_t colorMapSize;
        uint16_t xOrigin;
        uint16_t yOrigin;
        uint16_t width;
        uint16_t height;
        uint8_t pixelSize;
        uint8_t attributes;
    };

    boost::shared_ptr<SDL_Surface>
    loadTGA (const std::string &fileName)
    {
        enum
        {
            TGA_TYPE_RGB = 2,
            TGA_TYPE_RLE_RGB = 10
        };

        boost::function<uint16_t (uint16_t)> littleEndianShort (noSwap);
#if !defined (GP2X)
        bool bigEndian = isBigEndian ();
        if ( bigEndian )
        {
            littleEndianShort =
                boost::function<uint16_t (uint16_t)> (SDL_Swap16);
        }
#endif // !GP2X

        std::ifstream file (fileName.c_str (), std::ios::binary | std::ios::in);
        if ( !file )
        {
            std::string error ("Could not open graphic file: ");
            throw std::invalid_argument (error + fileName);
        }

        TGAHeader header;
        char headerData[18];
        char *headerDataPointer = headerData;
        file.read (headerData, 18);
        header.idLength = *(headerDataPointer + 0);
        header.colorMapType = *(headerDataPointer + 1);
        header.imageType = *(headerDataPointer + 2);
        header.colorMapIndex = *reinterpret_cast<uint16_t *>(headerDataPointer + 3);
        header.colorMapLength = *reinterpret_cast<uint16_t *>(headerDataPointer + 5);
        header.colorMapSize = *(headerDataPointer + 7);
        header.xOrigin = *reinterpret_cast<uint16_t *>(headerDataPointer + 8);
        header.yOrigin = *reinterpret_cast<uint16_t *>(headerDataPointer + 10);
        header.width = *reinterpret_cast<uint16_t *>(headerDataPointer + 12);
        header.height = *reinterpret_cast<uint16_t *>(headerDataPointer + 14);
        header.pixelSize = *(headerDataPointer + 16);
        header.attributes = *(headerDataPointer + 17);

        header.colorMapIndex = littleEndianShort (header.colorMapIndex);
        header.colorMapLength = littleEndianShort (header.colorMapLength);
        header.xOrigin = littleEndianShort (header.xOrigin);
        header.yOrigin = littleEndianShort (header.yOrigin);
        header.width = littleEndianShort (header.width);
        header.height = littleEndianShort (header.height);

        if ( TGA_TYPE_RGB != header.imageType &&
                TGA_TYPE_RLE_RGB != header.imageType )
        {
            std::string error ("Could not load `");
            error += fileName + "': Not an RGB image.";
            throw std::invalid_argument (error);
        }

#if defined(EMSCRIPTEN)
        if ( 0 != header.colorMapType ||
                (32 != header.pixelSize) )
        {
            std::string error ("Could not load `");
            error += fileName + "': Only 32-bit images are supported.";
            throw std::invalid_argument (error);
        }
#else
        if ( 0 != header.colorMapType ||
                (32 != header.pixelSize && 24 != header.pixelSize) )
        {
            std::string error ("Could not load `");
            error += fileName + "': Only 32 or 24 bit images are supported.";
            throw std::invalid_argument (error);
        }
#endif // !EMSCRIPTEN

        size_t bytesPerPixel = (header.pixelSize + 7) >> 3;
        bool alpha = 4 == bytesPerPixel;
        size_t width = header.width;
        size_t height = header.height;

        uint32_t amask = alpha ? 0xff000000 : 0;
        uint32_t rmask = 0x00ff0000;
        uint32_t gmask = 0x0000ff00;
        uint32_t bmask = 0x000000ff;
#if !defined (GP2X)
        if ( bigEndian )
        {
            uint8_t shift = alpha ? 0 : 8;
            amask = 0x000000ff >> shift;
            rmask = 0x0000ff00 >> shift;
            gmask = 0x00ff0000 >> shift;
            bmask = 0xff000000 >> shift;
        }
#endif // GP2X

        boost::shared_ptr<SDL_Surface> image (
                SDL_CreateRGBSurface (SDL_SWSURFACE, width, height,
                    bytesPerPixel * 8, rmask, gmask, bmask, amask),
                SDL_FreeSurface);
        if ( 0 == image.get () )
        {
            throw std::bad_alloc ();
        }

        file.seekg (header.idLength, std::ios_base::cur);

        if ( SDL_MUSTLOCK (image.get()) )
        {
            SDL_LockSurface (image.get());
        }
        ptrdiff_t stride = image->pitch;
        char *data = reinterpret_cast<char *> (image->pixels);
        // If the image is not stored from top to bottom, we need to change
        // the stride and data pointer.
        if ( 0x20 != (header.attributes & 0x20) )
        {
            data = data + (height - 1) * stride;
            stride = -stride;
        }

        bool rle = TGA_TYPE_RLE_RGB == header.imageType;
        size_t count = 0;
        size_t repetition = 0;
        for ( size_t y = 0 ; y < height ; ++y )
        {
            if ( rle )
            {
                uint32_t pixel;
                size_t x = 0;
                for ( ;; )
                {
                    if ( count > 0 )
                    {
                        size_t actualCount = std::min (count, width - x);
                        count -= actualCount;
                        file.read (data + x * bytesPerPixel,
                                actualCount * bytesPerPixel);
                        x += actualCount;
                        if ( x == width )
                        {
                            break;
                        }
                    }
                    else if ( repetition > 0 )
                    {
                        size_t actualRepetition =
                            std::min (repetition, width - x);
                        repetition -= actualRepetition;
                        while ( actualRepetition > 0 )
                        {
                            memcpy (data + x * bytesPerPixel, &pixel,
                                    bytesPerPixel);
                            ++x;
                            --actualRepetition;
                        }
                        if ( x == width )
                        {
                            break;
                        }
                    }

                    uint8_t rleData;
                    file.read (reinterpret_cast<char *> (&rleData), 1);
                    if ( 0x80 == (rleData & 0x80) )
                    {
                        repetition = (rleData & 0x7f) + 1;
                        file.read (reinterpret_cast<char *> (&pixel),
                                bytesPerPixel);
                    }
                    else
                    {
                        count = rleData + 1;
                    }
                }
            }
            else
            {
                file.read (data, width * bytesPerPixel);
            }
            data += stride;
        }
#if defined(EMSCRIPTEN)
        // Emscripten ignores the mask and shift bits, so it always uses RGB no matter what.
        uint32_t *idata = reinterpret_cast<uint32_t *> (image->pixels);
        for ( size_t y = 0 ; y < height ; ++y ) {
            for(size_t x = 0; x < width ; x++, idata ++) {
                uint32_t p = *idata;
                *idata = (p & 0xff000000) | ((p >> 16) & 0x000000ff) | (p & 0x0000ff00) | ((p << 16) & 0x00ff0000);
            }
            idata += image->pitch - width * 4;
        }
#endif // EMSCRIPTEN
        if ( SDL_MUSTLOCK (image.get()) )
        {
            SDL_UnlockSurface (image.get());
        }

        return image;
    }
}
