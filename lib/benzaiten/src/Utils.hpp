//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_UTILS_HPP)
#define GEISHA_STUDIOS_BENZAITEN_UTILS_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

namespace benzaiten
{
    ///
    /// @brief Clamps a value between a minimum and a a maximum.
    ///
    /// @param[in] value The value to clamp.
    /// @param[in] min The minimum value that @p value can be.
    /// @param[in] max The maximum value that @p value can be.
    ///
    /// @return @p max if @p value is greater than @p max,
    ///         @p min if @p value is less than @min, or
    ///         @p value otherwise.
    ///
    template <typename T>
    T
    clamp(const T &value, const T &min, const T &max)
    {
        if (value < min)
        {
            return min;
        }
        if (value > max)
        {
            return max;
        }
        return value;
    }
}

#endif // !GEISHA_STUDIOS_BENZAITEN_UTILS_HPP
