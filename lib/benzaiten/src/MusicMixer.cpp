//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "MusicMixer.hpp"
#include <cassert>

using namespace benzaiten;

MusicMixer::MusicMixer(boost::shared_ptr<Mix_Music> music):
    music_ (music)
{
    assert ( music != 0 && "Invalid music pointer" );
}

void
MusicMixer::halt()
{
    Mix_HaltMusic();
}

void
MusicMixer::pause()
{
    Mix_PauseMusic();
}

void
MusicMixer::play(int times)
{
    Mix_PlayMusic(music_.get (), times);
}

void
MusicMixer::resume()
{
    Mix_ResumeMusic();
}
