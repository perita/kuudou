//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_CONFIG_H
#include "System.hpp"
#include <stdexcept>
#include "ResourceManager.hpp"

using namespace benzaiten;

namespace
{
    /// @brief Does nothing to a surface.  Intended to be used with shared_ptr.
    void DoNotFreeSurface (SDL_Surface *) { }
}

System::System ()
{
    if ( SDL_Init (SDL_INIT_TIMER |
                SDL_INIT_AUDIO |
                SDL_INIT_VIDEO |
                SDL_INIT_JOYSTICK) < 0 )
    {
        throw Error (SDL_GetError ());
    }
}

System::~System ()
{
    SDL_Quit ();
#if defined (GP2X)
    chdir ("/usr/gp2x");
    execl ("/usr/gp2x/gp2xmenu", "/usr/gp2x/gp2xmenu", NULL);
#endif // GP2X
}

void
System::setIcon(Surface &icon)
{
    SDL_WM_SetIcon(icon.surface(), NULL);
}

void
System::setIcon(const ResourceManager &resources,
        const std::string &iconName)
{
#if !defined(APPLE) && !defined(GP2X) && !defined(A320) && !defined(PANDORA)
    try
    {
        Surface icon(resources.graphic(iconName));
        setIcon(icon);
    }
    catch(...)
    {
        // Ignore.  The icon is not that important...
    }
#endif // !APPLE && !GP2X && !A320 && !PANDORA
}

void
System::setTitle (const std::string &title)
{
    SDL_WM_SetCaption (title.c_str (), title.c_str ());
}

Surface
System::setVideoMode (int width, int height, int bitsperpixel,
        Surface::Flags flags)
{
    assert ( SDL_WasInit (SDL_INIT_VIDEO) );

#if defined (GP2X) || defined (A320) || defined (PANDORA)
    flags = static_cast<Surface::Flags>(flags | SDL_FULLSCREEN);
#endif // GP2X || A320 || PANDORA

    boost::shared_ptr<SDL_Surface> screen (
            SDL_SetVideoMode (width, height, bitsperpixel, flags),
            DoNotFreeSurface);
    if ( !screen )
    {
        throw Error (SDL_GetError ());
    }

#if defined (GP2X) || defined (A320) || defined (PANDORA)
    // This must be called *after* the call to SDL_SetVideoMode.
    SDL_ShowCursor (0);
#endif // GP2X || A320 || PANDORA

    return Surface (screen);
}
