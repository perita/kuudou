//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_EVENT_MANAGER_HPP)
#define GEISHA_STUDIOS_BENZAITEN_EVENT_MANAGER_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <map>
#include <boost/signals.hpp>
#include <SDL_events.h>
#include <SDL_joystick.h>
#if defined (A320)
#include "a320.hpp"
#endif // A320
#if defined (GP2X)
#include "gp2x.hpp"
#endif // GP2X
#if defined (EMSCRIPTEN)
#include <SDL_compat.h>
#endif // EMSCRIPTEN

namespace benzaiten
{
    ///
    /// @enum MenuAction
    /// @brief The menu's actions.
    ///
    enum MenuAction
    {
        Cancel,
        Select,
        Up,
        Down,
        Left,
        Right
    };

    ///
    /// @enum Axis
    /// @brief The joystick's axis.
    ///
    enum Axis
    {
        AxisNone,
        AxisDown,
        AxisLeft,
        AxisRight,
        AxisUp
    };

    ///
    /// @class EventManager
    /// @brief The manager for game events.
    ///
    class EventManager
    {
        public:
            /// The type for all events' handler.
            typedef boost::signals::connection Handler;

            /// The game should quit.
            mutable boost::signal<void ()> OnQuit;
            /// An action has started.
            mutable boost::signal<void (int)> OnActionBegin;
            /// An action has ended.
            mutable boost::signal<void (int)> OnActionEnd;
            /// A menu accion event.
            mutable boost::signal<void (MenuAction)> OnMenuAction;
            /// The application lost focus.
            mutable boost::signal<void()> OnLostFocus;

            ///
            /// @brief Initializes the joystick.
            ///
            EventManager ();

            ///
            /// @brief Uninitializes the joystick.
            ///
            ~EventManager ();

            ///
            /// @brief Maps a joystick button to an action.
            ///
            /// @param[in] button The button to map.
            /// @param[in] action The action to map @p button to.
            ///
            void mapJoy (uint8_t button, int action);

            ///
            /// @brief Maps a joystick axis to an action.
            ///
            /// @param[in] axis The axis to map.
            /// @param[in] action The action to map @p axis to.
            ///
            void mapJoyAxis(Axis axis, int action);

            ///
            /// @brief Maps a key to an action.
            ///
            /// @param[in] key The key symbol to map.
            /// @param[in] action The action to map the key to.
            ///
            void mapKey (SDLKey key, int action);

            ///
            /// @brief Fetches and processes all enqueued events.
            ///
            void update ();

        private:
            /// The type of a joy axis map.
            typedef std::map<Axis, int> JoyAxisMap;
            /// The type of a joy map.
            typedef std::map<uint8_t, int> JoyMap;
            /// The type of a key map.
            typedef std::map<SDLKey, int> KeyMap;

            ///
            /// @brief Sets the new Axis for X.
            ///
            /// @param[in] axis The axis to set to X.
            ///
            void setXAxis(Axis axis);

            ///
            /// @brief Sets the new Axis for Y.
            ///
            /// @param[in] axis The axis to set to Y.
            ///
            void setYAxis(Axis axis);

            /// Tells if GP2X click button is pressed.
            bool clickPressed_;
            /// The mapping between a joystick axis and an action.
            JoyAxisMap joyAxisMap_;
            /// The mapping between a joystick button and an action.
            JoyMap joyMap_;
            /// The joystick.
            SDL_Joystick *joystick_;
            /// The current axis on the X.
            Axis joyXAxis_;
            /// The current axis on the Y.
            Axis joyYAxis_;
            /// The mapping between a key and an action.
            KeyMap keyMap_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_EVENT_MANAGER_HPP
