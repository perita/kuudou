//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "ResourceManager.hpp"
#include "MusicNull.hpp"
#include "LoadTGA.hpp"
#include "SoundNull.hpp"

#if defined (WIN32)
#include <windows.h>
#endif // WIN32

#if defined (UNIX) && !defined (APPLE) && !defined (GP2X)
#include "binreloc.h"
#endif // UNIX && !APPLE && !GP2X

#if defined (EMSCRIPTEN)
// There is no SDL_DisplayFormat in Emscripten
#define SDL_DisplayFormat SDL_DisplayFormatAlpha
#endif // EMSCRIPTEN

using namespace benzaiten;

ResourceManager::ResourceManager (const std::string &appName):
    graphics_ (),
    graphicScaleFun_ (),
    musics_ (),
    resourcesPath_ (findResourcesPath (appName)),
    sounds_ ()
{
}

std::string
ResourceManager::findResourcesPath (const std::string &appName)
{
#if defined (APPLE) || defined (GP2X)
    // In Mac OS X we assume that the application's working directory is
    // the Resources directory, so we use the current directory as the
    // resource's path.
    return std::string (".");
#elif defined (EMSCRIPTEN)
    return std::string ("data");
#elif defined (UNIX)
#   if defined PACKAGE_DATA_DIR
    std::string resourcesPath (PACKAGE_DATA_DIR);
#   else // !PACKAGE_DATA_DIR
    std::string resourcesPath ("/usr/share");
#   endif // PACKAGE_DATA_DIR

    BrInitError error;
    if ( br_init (&error) == 0 && BR_INIT_ERROR_DISABLED != error )
    {
        // If binary relocation is disabled just return the hard coded path.
        // in resourcesPath.
    }
    else
    {
        resourcesPath = br_find_data_dir (resourcesPath.c_str ());
    }
    return resourcesPath + "/" + appName;
#elif defined (WIN32)
    // Initially assume that we are running in the executable's working path.
    std::string resourcesPath (".");

    // Now get the full path to the executable.
    TCHAR executableFile[MAX_PATH];
    if ( GetModuleFileName (0, executableFile, MAX_PATH) > 0 )
    {
        std::string executablePath (executableFile);
        std::string::size_type backslashPos = executablePath.rfind ("\\");
        if ( std::string::npos != backslashPos )
        {
            // If we can find it, append the relative resources path
            // to the absolute executable's path to make it an absolute
            // path (more or less... probably it will fail if we get
            // past the MAX_PATH limit somewhere.)
            executablePath.erase (backslashPos);
            resourcesPath = executablePath + "\\" + resourcesPath;
        }
    }

    return resourcesPath;
#else
#error Unknown platform.
#endif
}

std::string
ResourceManager::getFilePath (const std::string &directory,
        const std::string &fileName) const
{
    return joinPath (joinPath (getResourcesPath (), directory), fileName);
}

std::string
ResourceManager::getResourcesPath () const
{
    return resourcesPath_;
}

Surface
ResourceManager::graphic (const std::string &fileName) const
{
    Graphic graphic (graphics_[fileName]);
    GraphicPtr graphicPtr (graphic.lock ());
    if ( !graphicPtr )
    {
        graphicPtr = loadGraphicFile (getFilePath ("gfx", fileName));
        if ( graphicScaleFun_ )
        {
            graphicPtr = GraphicPtr (graphicScaleFun_ (graphicPtr.get ()),
                    SDL_FreeSurface);
        }
        graphics_[fileName] = graphicPtr;
    }
    return Surface (graphicPtr);
}

std::string
ResourceManager::joinPath (const std::string &path0, const std::string &path1)
{
#if defined (WIN32)
    const char separator = '\\';
#else // !WIN32
    const char separator = '/';
#endif // WIN32

    return path0 + separator + path1;
}

ResourceManager::GraphicPtr
ResourceManager::loadGraphicFile (const std::string &fileName)
{
    GraphicPtr tga (loadTGA (fileName));

    bool hasAlpha = 32 == tga->format->BitsPerPixel;
    GraphicPtr tgaOptimized;
    if ( hasAlpha )
    {
        tgaOptimized = GraphicPtr (SDL_DisplayFormatAlpha (tga.get ()), SDL_FreeSurface);
    }
    else
    {
        SDL_SetColorKey (tga.get (), SDL_SRCCOLORKEY | SDL_RLEACCEL,
                SDL_MapRGB (tga->format, 255, 0, 255));
        tgaOptimized = GraphicPtr (SDL_DisplayFormat (tga.get ()), SDL_FreeSurface);
    }

    if ( !tgaOptimized )
    {
        // If we reach here it means that we could load the TGA but we couldn't
        // "optimize" it using SDL_DisplayFormat.  From my point of view it is
        // better to return the original TGA as "optimized" instead of failing.
        tgaOptimized = tga;
    }

    return tgaOptimized;
}

ResourceManager::MusicPtr
ResourceManager::loadMusicFile (const std::string &fileName)
{
#if !defined (EMSCRIPTEN)
    // Check to see if SDL_mixer is loaded.
    {
        int frequency;
        int channels;
        uint16_t format;
        if ( 0 == Mix_QuerySpec (&frequency, &format, &channels) )
        {
            throw std::invalid_argument ("Can't open music without the audio system.");
        }
    }
#endif // !EMSCRIPTEN

    MusicPtr music (Mix_LoadMUS (fileName.c_str ()), Mix_FreeMusic);
    if ( !music )
    {
        throw std::invalid_argument (std::string ("Error loading music ") +
                fileName + ": " + Mix_GetError ());
    }
    return music;
}

ResourceManager::SoundPtr
ResourceManager::loadSoundFile (const std::string &fileName)
{
#if !defined (EMSCRIPTEN)
    // Check to see if SDL_mixer is loaded.
    {
        int frequency;
        int channels;
        uint16_t format;
        if ( 0 == Mix_QuerySpec (&frequency, &format, &channels) )
        {
            throw std::invalid_argument ("Can't open sound without the audio system.");
        }
    }
#endif // !EMSCRIPTEN

    SoundPtr sound (Mix_LoadWAV (fileName.c_str ()), Mix_FreeChunk);
    if ( !sound )
    {
        throw std::invalid_argument (std::string ("Error loading sound ") +
                fileName + ": " + Mix_GetError ());
    }
    return sound;
}

MusicMixer
ResourceManager::music (const std::string &fileName) const
{
    MusicRes music (musics_[fileName]);
    MusicPtr musicPtr (music.lock ());
    if ( !musicPtr )
    {
        musicPtr = loadMusicFile (getFilePath ("music", fileName));
        musics_[fileName] = musicPtr;
    }
    return MusicMixer(musicPtr);
}

void
ResourceManager::setGraphicScaleFunction (GraphicScaleFun function)
{
    graphicScaleFun_ = function;
    graphics_.clear ();
}

SoundMixer
ResourceManager::sound (const std::string &fileName) const
{
    SoundRes sound (sounds_[fileName]);
    SoundPtr soundPtr (sound.lock ());
    if ( !soundPtr )
    {
        soundPtr = loadSoundFile (getFilePath ("sfx", fileName));
        sounds_[fileName] = soundPtr;
    }
    return SoundMixer(soundPtr);
}

Music::ptr
ResourceManager::try_music(const std::string &fileName) const
{
    try
    {
        return Music::ptr(new MusicMixer(music(fileName)));
    }
    catch (std::invalid_argument &)
    {
        return Music::ptr(new MusicNull());
    }
}

Sound::ptr
ResourceManager::try_sound(const std::string &fileName) const
{
    try
    {
        return Sound::ptr(new SoundMixer(sound(fileName)));
    }
    catch (std::invalid_argument &)
    {
        return Sound::ptr(new SoundNull());
    }
}
