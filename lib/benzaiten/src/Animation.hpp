//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_ANIMATION_HPP)
#define GEISHA_STUDIOS_BENZAITEN_ANIMATION_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include "SpriteSheet.hpp"

namespace benzaiten
{
    ///
    /// @class Animation.
    /// @brief An animation sequence over an sprite sheet.
    ///
    class Animation
    {
        public:
            ///
            /// @enum Direction
            /// @brief The animation's direction.
            ///
            enum Direction
            {
                Backward = -1,
                Forward = 1
            };

            ///
            /// @brief Initializes an animation.
            ///
            /// @param[in] spriteSheet the SpriteSheet to use to get the
            ///            animation sequence.
            /// @param[in] startIndex The first SpriteSheet index of the
            ///            animation.
            /// @param[in] count The number of frames this animation has.
            /// @param[in] speed The speed of the animation.  If an animation
            ///            has an \p speed of 1, then each nextFrame() will
            ///            change its frame.  If it has an speed of 2, then
            ///            it will need to call nextFrame() twice to get
            ///            the next frame, an so on.
            /// @param[in] loop Tells if the animation is a loop, or just
            ///            an one time sequence.
            /// @param[in] direction The direction of the sequence.  If \c
            ///            Forward then it will go from \p startIndex to
            ///            \p startIndex + \p count - 1.  Otherwise it will
            ///            go from \p startIndex to \p startIndex - \p count +
            ///            1.
            ///
            Animation (const SpriteSheet &spriteSheet, unsigned int startIndex,
                    unsigned int count = 1, unsigned int speed = 1,
                    bool loop = true, Direction direction = Forward);

            ///
            /// @brief Draws the current's animation frame.
            ///
            /// @param[in] x The X position to draw the current frame.
            /// @param[in] y The Y position to draw the current frame.
            /// @param[in] screen The surface to draw the current frame to.
            ///
            /// @return The rectangle where the animation has been drawn to.
            ///
            SDL_Rect draw (int16_t x, int16_t y, Surface &screen) const;

            ///
            /// @brief Selects the animation to the first frame.
            ///
            void firstFrame ();

            ///
            /// @brief Tells if the last frame has been selected.
            ///
            /// @return @c true for non-loop animations that its last
            ///         frame has been already selected in lastFrame()
            ///         or nextFrame() and another call to nextFrame() followed.
            ///
            bool hasEnded () const;

            ///
            /// @brief Tells the animation's height.
            ///
            /// @return The animation's height.
            ///
            size_t height () const;

            ///
            /// @brief Selects the animation's last frame.
            ///
            void lastFrame ();

            ///
            /// @brief Selects the animation's next frame.
            ///
            void nextFrame ();

            ///
            /// @brief Sets the animation's alpha value.
            ///
            /// @param[in] alpha The alpha value to set to the animation.
            ///
            void setAlpha (uint8_t alpha);

            ///
            /// @brief Tells the animation's width.
            ///
            /// @return The animation's width.
            ///
            size_t width () const;

        private:
            ///
            /// @brief Gets the reference to the sprite's sheet.
            ///
            /// @return The reference to the sprite's sheet.
            ///
            SpriteSheet &spriteSheet ();

            ///
            /// @brief Gets the constant reference to the sprite's heet.
            ///
            /// @return The constant reference to the sprite's sheet.
            ///
            const SpriteSheet &spriteSheet () const;

            /// The first frame index.
            unsigned int begin_;
            /// The current frame.
            unsigned int currentFrame_;
            /// Tells if the animation has ended.
            bool ended_;
            /// The animation's direction.
            Direction direction_;
            /// Past one of the last frame.
            unsigned int end_;
            /// Tells if looping the animation.
            bool loop_;
            /// The animation's speed.
            unsigned int speed_;
            /// The sprite sheet to use to draw the animation.
            SpriteSheet spriteSheet_;
            /// The current animation's time.
            unsigned int time_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_ANIMATION_HPP
