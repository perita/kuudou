Ludum Dare 22
=============
Jordi “summaky” Fita <jfita@geishastudios.com>
December 2011

The main protagonist in “kuudou” is a “lone lorn creetur” trapped in a big
empty cave.  As with most cave dwellers, this creature has evolved to navigate
the cave as easily as possible; in her case she learnt to glow her own body and
see her way through out the cave.  Unfortunately, the range is quite limited
and proves to be less useful than what would seem a priori.

However, what at first looked like a deserted cave, it is actually full of
life.  There are other critters that are wandering--quite stupidly--all around
the cave.  Although our creature can't see them standing at a distance, their
eyes are distinctly bright inside the darkness inside.

The idea is to look for clues from the other critters by observing their
movements and guessing where the platforms are located at.  Other times, it is
necessary to be brave and explore a little taking advantage of the body's glow.

To play the game you can use the keyboard or a joystick.  I've only tested it
with a XBOX360 joystick, but should work on others as well.  The keyboards
controls are the left and right arrow keys to move the creature.  Up arrow key
or space is to jump.  Q or Escape closes the game.  If you prefer, it is
possible to use vi-like keys: `h' and `l' to move; `k' to jump.

The game is short; there are only four stages.  And when it is over it returns
nonchalantly to the title screen.

The game's name, “kuudou”, means “empty cave”.  I used this idea for the alone
theme because I was playing with some dark colors schemes while I was trying to
come up with a plausible idea.  I don't know why, the song “kuudou desu” by
”yura yura teikoku” came to my mind.  And I decided to try to make this game as
a metaphor, as it were, that being alone is actually not seeing others.

Unfortunately, the game fails at being both a metaphor and fun.  The game
mechanics are more frustrating that anything else.  And the fact that some
jumps require almost pixel-perfect precision doesn't help either.  I believe
that all the levels are solvable with what is “seen” on screen; either with the
clues from the critter's eyes or jumping around a little (without falling).
The stages aren't well balanced, thought, and are somewhat contrived.
