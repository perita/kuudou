//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "TitleState.hpp"
#include <BitmapFont.hpp>
#include <GameStateManager.hpp>
#include <ResourceManager.hpp>
#include "Actions.hpp"
#include "ListState.hpp"

using namespace ld22;

namespace {
    const float BLINK_TIME = 1000;
    const int TITLE_Y = (SCREEN_HEIGHT / 4) * SCREEN_SCALE;
    const int LABEL_Y = (SCREEN_HEIGHT * 3 / 4) * SCREEN_SCALE;

    benzaiten::Surface convert_surface (const benzaiten::Surface &source) {
        using benzaiten::Surface;
        Surface output (source.width (), source.height (), Surface::screen ());
        source.blit (output);
        return output;
    }

    benzaiten::Surface make_label (const benzaiten::ResourceManager &resources) {
        using namespace benzaiten;
        BitmapFont font (L" !\"#$%'(),.0123456789:;<=>?ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^abcdefghijklmnopqrstuvwxyz", resources.graphic ("font.tga"));
        std::string text ("Press SPACE or JOYSTICK BUTTON to start");
        Surface label (font.width (text), font.height (), Surface::screen ());
        label.fill (0, 0, 0);
        font.draw (text, 0, 0, label);
        return label;
    }
}

BENZAITEN_EVENT_TABLE (TitleState)
{
    BENZAITEN_EVENT1 (OnActionBegin, &TitleState::on_action_begin);
}

TitleState::TitleState (const benzaiten::ResourceManager &resources, bool debug):
    alpha (0.0),
    blink_time (0.0),
    debug (debug),
    label (make_label (resources)),
    resources (resources),
    show_text (false),
    title (convert_surface (resources.graphic ("title.tga")))
{
}

void TitleState::draw (benzaiten::Surface &screen) {
    screen.fill (0, 0, 0);
    this->title.setAlpha (std::min (static_cast<int>(this->alpha), 255));
    this->title.blit (screen.width () / 2 - this->title.width () / 2, TITLE_Y, screen);
    if (this->show_text) {
        this->label.blit (screen.width () / 2 - this->label.width () / 2, LABEL_Y, screen);
    }
}

void TitleState::on_action_begin (int action) {
    switch (action) {
        case Jump:
            this->alpha = 255.0;
            this->stateManager ().setActiveState (
                    IGameState::ptr (new ListState (this->resources, this->debug)));
        case Exit:
            this->stateManager ().removeActiveState ();
            break;
    }
}

void TitleState::update (float elapsed_time) {
    if (this->alpha < 255) {
        this->alpha += 2.25;
    } else {
        this->blink_time -= elapsed_time;
        if (this->blink_time < 0.0) {
            this->show_text = !this->show_text;
            this->blink_time = BLINK_TIME;
        }
    }
}
