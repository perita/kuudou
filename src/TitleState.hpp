//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_LD22_TITLE_STATE_HPP)
#define GEISHA_STUDIOS_LD22_TITLE_STATE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <IGameState.hpp>
#include <Surface.hpp>

namespace benzaiten {
    class ResourceManager;
}

namespace ld22
{
    class TitleState: public benzaiten::IGameState {
        public:
            TitleState (const benzaiten::ResourceManager &resources, bool debug);

            virtual void draw (benzaiten::Surface &screen);
            virtual void update (float elapsed_time);

        private:
            void on_action_begin (int action);

            double alpha;
            float blink_time;
            bool debug;
            benzaiten::Surface label;
            const benzaiten::ResourceManager &resources;
            bool show_text;
            benzaiten::Surface title;

            DECLARE_BENZAITEN_EVENT_TABLE ();
    };
}

#endif // !GEISHA_STUDIOS_LD22_TITLE_STATE_HPP
