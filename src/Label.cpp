//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Label.hpp"
#include <BitmapFont.hpp>

using namespace ld22;

Label::Label (int16_t y,  const std::string &text, benzaiten::BitmapFont &font):
    fade_in (255),
    fade_out (255),
    label (font.width (text), font.height(), benzaiten::Surface::screen ()),
    read_time (3000),
    y (y * SCREEN_SCALE)
{
    label.fill (0, 0, 0);
    font.draw (text, 0, 0, label);
}

void Label::draw (benzaiten::Surface &screen) {
    if (this->fade_in > 0) {
        this->label.setAlpha (255 - this->fade_in);
    } else {
        this->label.setAlpha (this->fade_out);
    }
    this->label.blit (screen.width () / 2 - this->label.width() / 2, this->y, screen);
}

bool Label::update (float elapsed_time) {
    if (this->fade_in > 0) {
        this->fade_in -= 3;
    } else if (this->read_time > 0) {
        this->read_time -= elapsed_time;
    } else if (this->fade_out > 0) {
        this->fade_out -= 3;
    }
    return this->fade_out > 0;
}
