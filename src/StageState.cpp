//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "StageState.hpp"
#include <algorithm>
#include <cmath>
#include <fstream>
#include <functional>
#include <stdexcept>
#include <GameStateManager.hpp>
#include <ResourceManager.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include "Actions.hpp"

using namespace ld22;

namespace
{
    const int16_t LABEL_POS = 10;
    const int16_t DOOR_MARGIN = 4;

    enum EnemyTypes {
        ENEMY_BAT = 0,
        ENEMY_FIREFLY = 1,
        ENEMY_TURTLE = 2
    };

    enum PlatformType {
        PLATFORM_STATIC = 0
    };

    void check_stream (std::istream &stream, const std::string what) {
        if (!stream || stream.eof ()) {
            throw std::runtime_error (
                (boost::format ("Couldn't read %1% from stage file") % what).str ());
        }
    }

    bool collide (const SDL_Rect &lhs, const SDL_Rect &rhs) {
        int max_x = std::min (lhs.x + lhs.w, rhs.x + rhs.w);
        int min_x = std::max (lhs.x, rhs.x);
        int max_y = std::min (lhs.y + lhs.h, rhs.y + rhs.h);
        int min_y = std::max (lhs.y, rhs.y);
        return (max_x - min_x > 0) && (max_y - min_y > 0);
    }

    char skip_comments (std::istream &stream) {
        char directive;
        stream >> directive;
        while (directive == '#') {
            std::string line;
            std::getline (stream, line);
            // Ignore the line like women ignore me…
            stream >> directive;
        }
        return directive;
    }

    void expecting (char directive, char expected) {
        if (directive != expected) {
            throw std::runtime_error (
                (boost::format ("Found `%1%' but `%2%' was expected") % directive % expected).str ());
        }
    }

    // -- The functions that make enemies from the file.
    StageState::EnemyPtr make_enemy_bat (std::istream &file,
            const benzaiten::ResourceManager &resources) {
        int from_x;
        check_stream (file >> from_x, "bat start x");
        int from_y;
        check_stream (file >> from_y, "bat start y");
        int to_x;
        check_stream (file >> to_x, "bat end x");
        int to_y;
        check_stream (file >> to_y, "bat end y");
        double speed;
        check_stream (file >> speed, "bat speed");
        return StageState::EnemyPtr (
                new Bat (from_x, from_y, to_x, to_y, speed, resources));
    }

    StageState::EnemyPtr make_enemy_firefly (std::istream &file,
            const benzaiten::ResourceManager &resources) {
        int x;
        check_stream (file >> x, "firefly x");
        int y;
        check_stream (file >> y, "firefly y");
        double speed;
        check_stream (file >> speed, "firefly speed");
        return StageState::EnemyPtr (new Firefly (x, y, speed, resources));
    }

    StageState::EnemyPtr make_enemy_turtle (std::istream &file,
            const StageState::Platforms &platforms,
            const benzaiten::ResourceManager &resources) {
        int platform_index;
        check_stream (file >> platform_index, "turtle platform");
        if (platform_index >= platforms.size ()) {
            throw std::runtime_error (
                    (boost::format ("can't place turtle to platform %1%") %
                        platform_index).str ());
        }
        const Platform &platform (platforms.at (platform_index));
        SDL_Rect rect (platform.get_rect ());
        int16_t from_x = rect.x;
        int16_t to_x = rect.x + rect.w;
        int16_t y = rect.y;

        double x_percent;
        check_stream (file >> x_percent, "turtle %%x");
        int16_t x = std::min (to_x,
                static_cast<int16_t>(from_x + x_percent * (to_x - from_x) / 100.0 + 0.5));

        double speed;
        check_stream (file >> speed, "turtle speed");
        return StageState::EnemyPtr (new Turtle (from_x, to_x, x, y, speed, resources));
    }

    // -- The functions that make platforms from the file.
    Platform make_platform_static (std::istream &file,
            const benzaiten::ResourceManager &resources) {
        int x;
        check_stream (file >> x, "static platform x");
        int y;
        check_stream (file >> y, "static platform y");
        int width;
        check_stream (file >> width, "static platform width");
        int height;
        check_stream (file >> height, "static platform height");

        benzaiten::Surface image (resources.graphic ("blocks.tga"));
        benzaiten::SpriteSheet sheet (image, image.width ());
        return Platform (benzaiten::Animation (sheet, 0, 5, 0), x, y, width, height);
    }
}

BENZAITEN_EVENT_TABLE (StageState)
{
    BENZAITEN_EVENT1 (OnActionBegin, &StageState::on_action_begin);
    BENZAITEN_EVENT1 (OnActionEnd, &StageState::on_action_end);
}

StageState::StageState (const std::string &file_name,
        const benzaiten::ResourceManager &resources,
        bool debug):
    background_color (debug ? 255 : 0, debug ? 255 : 0, debug ? 255 : 0),
    current_text (),
    enemies (),
    exit_door (resources),
    font (L" !\"#$%'(),.0123456789:;<=>?ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^abcdefghijklmnopqrstuvwxyz",
            resources.graphic ("font.tga")),
    label (),
    platforms (),
    player (resources),
    strings ()
{
    std::ifstream file (file_name.c_str ());
    if (!file) {
        throw std::runtime_error ("Couldn't load stage " + file_name);
    }

    // The starting position.
    {
        expecting (skip_comments (file), '>');
        int x;
        check_stream (file >> x, "player start x");
        this->player.set_x (x);
        int y;
        check_stream (file >> y, "player start y");
        this->player.set_y (y);
        expecting (skip_comments (file), '>');
        check_stream (file >> x, "exit x");
        check_stream (file >> y, "exit y");
        this->exit_door.move (x, y);
        expecting (skip_comments (file), '@');
    }

    // The strings
    while (true) {
        char directive = skip_comments (file);
        if (directive == '@') {
            break;
        }
        expecting (directive, '>');
        std::string line;
        check_stream (std::getline (file, line), "display string");
        std::string::size_type index = line.find_first_of ('|');
        while (index != std::string::npos) {
            line[index] = '\n';
            index = line.find_first_of ('|', index + 1);
        }
        this->strings.push_back (line);
    }
    this->current_text = this->strings.begin ();

    // The platforms
    while (true) {
        char directive = skip_comments (file);
        if (directive == '@') {
            break;
        }
        expecting (directive, '>');
        int type;
        check_stream (file >> type, "platforms type");
        switch (type) {
            case PLATFORM_STATIC:
                this->platforms.push_back (make_platform_static (file, resources));
                break;

            default:
                throw std::runtime_error (
                    (boost::format ("Platform type `%1%' unknown") % type).str ());
                break;
        }
    }

    // The enemies
    while (true) {
        char directive = skip_comments (file);
        if (directive == '@') {
            break;
        }
        expecting (directive, '>');
        int type;
        check_stream (file >> type, "enemy type");
        switch (type) {
            case ENEMY_BAT:
                this->enemies.push_back (make_enemy_bat (file, resources));
                break;

            case ENEMY_FIREFLY:
                this->enemies.push_back (make_enemy_firefly (file, resources));
                break;

            case ENEMY_TURTLE:
                this->enemies.push_back (
                        make_enemy_turtle (file, this->platforms, resources));
                break;

            default:
                throw std::runtime_error (
                    (boost::format ("Enemy type `%1%' unknown") % type).str ());
                break;
        }
    }
}

void StageState::draw (benzaiten::Surface &screen) {
    screen.fill (this->background_color);

    this->player.draw_halo (screen);
    std::for_each (this->enemies.begin (), this->enemies.end (),
            boost::bind (&Enemy::draw, _1, screen));
    this->exit_door.draw (screen);
    std::for_each (this->platforms.begin (), this->platforms.end (),
            boost::bind (&Platform::draw, _1, screen));
    this->player.draw_character (screen);

    if (this->label != 0) {
        this->label->draw (screen);
    }
}

void StageState::on_action_begin (int action) {
    switch (action) {
        case Exit:
            do {
                this->stateManager ().removeActiveState (benzaiten::Fade::None);
            } while (this->stateManager ().hasActiveState ());
            break;

        case Jump:
            this->player.jump ();
            break;

        case MoveLeft:
            this->player.begin_move_left ();
            break;

        case MoveRight:
            this->player.begin_move_right ();
            break;
    }
}

void StageState::on_action_end (int action) {
    switch (action) {
        case MoveLeft:
            this->player.end_move_left ();
            break;

        case MoveRight:
            this->player.end_move_right ();
            break;
    }
}

void StageState::update (float elapsed_time) {
    // First move only the X coordinate.
    SDL_Rect old_player_rect = this->player.get_rect ();
    this->player.update (elapsed_time, false);
    SDL_Rect player_rect = this->player.get_rect ();
    int offset_x = player_rect.x - old_player_rect.x;
    BOOST_FOREACH (Platform platform, this->platforms) {
        SDL_Rect platform_rect = platform.get_rect ();
        if (collide (player_rect, platform_rect)) {
            if (offset_x > 0) {
                this->player.set_x (platform_rect.x, Player::RIGHT_HAND);
            } else if (offset_x < 0) {
                this->player.set_x (platform_rect.x + platform_rect.w, Player::LEFT_HAND);
            }
        }
    }

    // Now the Y coordinate.
    old_player_rect = this->player.get_rect ();
    this->player.update (elapsed_time, true);
    player_rect = this->player.get_rect ();
    int offset_y = player_rect.y - old_player_rect.y;

    // This test requires a taller height to prevent the player to “enter” the blocks.
    player_rect.h++;
    // This is the default because if we moved on Y then only if she collides
    // the feet on a platform is on floor.
    this->player.set_on_floor (false);
    BOOST_FOREACH (Platform platform, this->platforms) {
        SDL_Rect platform_rect = platform.get_rect ();
        // I need to correct the player's position, because the player
        // knows nothing about platforms and doesn't even know that it
        // has collided.  I move her depending on whether she was
        // jumping (move her below the platform) or was falling (place
        // her over the platform.)  When falling, additionaly, I have to tell
        // her that she is on a platform.
        if (collide (player_rect, platform_rect)) {
            if (offset_y >= 0) {
                this->player.set_y (platform_rect.y, Player::FEET);
                this->player.set_on_floor (true);
            } else {
                this->player.set_y (platform_rect.y + platform_rect.h, Player::HEAD);
                this->player.stop_jump ();
            }
        }
    }

    // Create a new label if there is none and there are more text to show.
    if (this->label == 0) {
        if (this->current_text != this->strings.end ()) {
            this->label.reset (new Label (LABEL_POS, *(this->current_text), this->font));
            ++this->current_text;
        }
    } else {
        if (!this->label->update (elapsed_time)) {
            this->label.reset (0);
        }
    }

    std::for_each (this->enemies.begin (), this->enemies.end (),
            boost::bind (&Enemy::update, _1, elapsed_time, this->player));

    // If the player is below the screen (look under the table!) then the stage is lost.
    player_rect = this->player.get_rect ();
    if (this->player.is_on_floor ()) {
        // Make the rect slightly narrower so the player “enters” it.
        SDL_Rect door_rect = this->exit_door.get_rect ();
        door_rect.x += DOOR_MARGIN;
        door_rect.w -= 2 * DOOR_MARGIN;
        if (collide (player_rect, door_rect)) {
            this->player_escaped ();
            this->stateManager ().removeActiveState ();
        }
    } else if (player_rect.y > SCREEN_HEIGHT) {
        this->stateManager ().removeActiveState ();
    }
}
