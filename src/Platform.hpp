//
// Ludum Dare 21
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_LD22_PLATFORM_HPP)
#define GEISHA_STUDIOS_LD22_PLATFORM_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <Surface.hpp>

namespace benzaiten
{
    class Animation;
}

namespace ld22
{
    class Platform
    {
        public:
            Platform (const benzaiten::Animation &animation, int16_t x, int16_t y,
                    unsigned int width, unsigned int height);

            void draw (benzaiten::Surface &screen);

            SDL_Rect get_rect() const {
                SDL_Rect rect = { Sint16(this->x), Sint16(this->y), Uint16(this->width * 4), Uint16(this->height * 4) };
                return rect;
            }

        private:
            unsigned int height;
            benzaiten::Surface surface;
            unsigned int width;
            int16_t x;
            int16_t y;
    };
}

#endif // !GEISHA_STUDIOS_LD22_PLATFORM_HPP
