//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Player.hpp"
#include <ResourceManager.hpp>

using namespace ld22;

namespace {
    // The gravity pulling down the player at px/frame.
    const double GRAVITY = -0.05;

    // The initial thrust when jumping in ps/frame.
    const double JUMP_THRUST = -1.75;

    // The player's speed in px/frame.
    const double PLAYER_SPEED_X = 0.75;

    enum Animations {
        STAND_LEFT = 0,
        STAND_RIGHT = 1,
        WALK_LEFT = 2,
        WALK_RIGHT = 3
    };

    benzaiten::Sprite create_sprite (const benzaiten::ResourceManager &resources) {
        using benzaiten::Animation;
        using benzaiten::Surface;
        using benzaiten::Sprite;
        using benzaiten::SpriteSheet;

        Surface mainchar = resources.graphic ("mainchar.tga");
        SpriteSheet sheet_right (mainchar, mainchar.width (), mainchar.height () / 3);
        SpriteSheet sheet_left (benzaiten::flip (mainchar, true, false),
                sheet_right.width (), sheet_right.height ());
        // Follow the animations enumeration.
        Sprite::Animations animations;
        animations.push_back (Animation (sheet_left, 0, 1));
        animations.push_back (Animation (sheet_right, 0, 1));
        animations.push_back (Animation (sheet_left, 1, 2, 5));
        animations.push_back (Animation (sheet_right, 1, 2, 5));
        return Sprite (animations);
    }
}

Player::Player (const benzaiten::ResourceManager &resources):
    direction_x (NONE),
    direction_y (0.0),
    on_floor (true),
    spotlight (resources.graphic ("spotlight.tga")),
    spotlight_offset_x (spotlight.width () / 2),
    spotlight_offset_y (spotlight.height () / 2),
    sprite (create_sprite(resources)),
    sprite_offset_x (sprite.width () / 2),
    sprite_offset_y (sprite.height () / 2),
    width (sprite.width () / SCREEN_SCALE),
    height (sprite.height () / SCREEN_SCALE),
    x (0),
    y (0)
{
}

void Player::begin_move_left () {
    this->direction_x = LEFT;
    this->sprite.setAnimation (WALK_LEFT);
}

void Player::begin_move_right () {
    this->direction_x = RIGHT;
    this->sprite.setAnimation (WALK_RIGHT);
}

void Player::end_move_left () {
    if (this->direction_x == LEFT) {
        this->direction_x = NONE;
        this->sprite.setAnimation (STAND_LEFT);
    }
}

void Player::end_move_right () {
    if (this->direction_x == RIGHT) {
        this->direction_x = NONE;
        this->sprite.setAnimation (STAND_RIGHT);
    }
}

void Player::draw_character (benzaiten::Surface &screen) {
    int final_x = this->x * SCREEN_SCALE;
    int final_y = this->y * SCREEN_SCALE;

    sprite.draw (final_x - this->sprite_offset_x,
            final_y - this->sprite_offset_y, screen);
}

void Player::draw_halo (benzaiten::Surface &screen) {
    int final_x = this->x * SCREEN_SCALE;
    int final_y = this->y * SCREEN_SCALE;

    int spotlight_x = final_x - this->spotlight_offset_x;
    int spotlight_y = final_y - this->spotlight_offset_y;
    screen.fill (127, 127, 127, 255, spotlight_x, spotlight_y,
            this->spotlight.width (), this->spotlight.height ());
    this->spotlight.blit (spotlight_x, spotlight_y, screen);
}

bool Player::is_on_floor () const {
    return this->on_floor;
}

void Player::jump () {
    if (this->is_on_floor ()) {
        this->direction_y = JUMP_THRUST;
        this->set_on_floor (false);
    }
}

void Player::set_on_floor (bool on_floor) {
    this->on_floor = on_floor;
}

void Player::set_x (int x, HPOI center_on) {
    switch (center_on) {
        case LEFT_HAND:
            this->x = x + this->width / 2;
            break;

        case RIGHT_HAND:
            this->x = x - this->width / 2;
            break;

        default:
            this->x = x;
            break;
    }
}

void Player::set_y (int y, VPOI center_on) {
    switch (center_on) {
        case HEAD:
            this->y = y + this->height / 2;
            break;

        case FEET:
            this->y = y - this->height / 2;
            break;

        default:
            this->y = y;
            break;
    }
}

void Player::stop_jump () {
    if (this->direction_y < 0.0) {
        this->direction_y = 0.0;
    }
}

void Player::update (float elapsed_time, bool fall_only) {
    if (fall_only) {
        if (!this->is_on_floor ()) {
            this->y += this->direction_y;
            this->direction_y -= GRAVITY;
        }
    } else {
        if (this->is_on_floor ()) {
            this->sprite.nextFrame ();
        }
        this->x += this->direction_x * PLAYER_SPEED_X;
    }
}
