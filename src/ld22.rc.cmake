#include <windows.h>
LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
#pragma code_page(1252)

A ICON "@PROJECT_SOURCE_DIR@/data/gfx/icon.ico"

VS_VERSION_INFO VERSIONINFO
    FILEVERSION @VERSION_MAJOR@, @VERSION_MINOR@, @VERSION_PATCH@, @VERSION_REV@
    PRODUCTVERSION @VERSION_MAJOR@, @VERSION_MINOR@, @VERSION_PATCH@, @VERSION_REV@
    FILEFLAGSMASK VS_FFI_FILEFLAGSMASK
#if defined(_DEBUG)
    FILEFLAGS VS_FF_DEBUG
#else // !_DEBUG
    FILEFLAGS 0
#endif
    FILEOS VOS__WINDOWS32
    FILETYPE VFT_APP
    FILESUBTYPE VFT_UNKNOWN
    BEGIN
        BLOCK "StringFileInfo"
        BEGIN
            BLOCK "040904b0"
            BEGIN
                VALUE "CompanyName", "Geisha Studios"
                VALUE "FileDescription", "LD22 Entry; theme: Alone"
                VALUE "FileVersion", "@VERSION@"
                VALUE "InternalName", "@PROJECT_NAME@"
                VALUE "LegalCopyright", "Copyright (c) 2011 Geisha Studios"
                VALUE "OriginalFileName", "ld22.exe"
                VALUE "ProductName", "@MACOSX_BUNDLE_BUNDLE_NAME@"
                VALUE "ProductVersion", "@VERSION@"
            END
        END
        BLOCK "VarFileInfo"
        BEGIN
            VALUE "Translation", 0x409, 1200
        END
    END
