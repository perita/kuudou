//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "ListState.hpp"
#include <fstream>
#include <stdexcept>
#include <GameStateManager.hpp>
#include <ResourceManager.hpp>
#include <boost/bind.hpp>
#include "StageState.hpp"

using namespace ld22;

ListState::ListState (const benzaiten::ResourceManager &resources, bool debug):
    current_stage (),
    debug (debug),
    resources (resources),
    stages ()
{
    std::ifstream file (this->resources.getFilePath ("stages", "stages.lst").c_str ());
    if (!file) {
        throw std::runtime_error ("Couldn't open stages list file");
    }
    while (!file.eof () && file) {
        std::string stage_file_name;
        std::getline (file, stage_file_name);
        if (file && stage_file_name != "") {
            this->stages.push_back (stage_file_name);
        }
    }
    this->current_stage = this->stages.begin ();
}

void ListState::draw (benzaiten::Surface &screen) {
    // Nothind to do
}

void ListState::next_stage () {
    if (this->current_stage != this->stages.end()) {
        ++this->current_stage;
    }
}

void ListState::update (float elapsed_time) {
    if (this->current_stage == this->stages.end ()) {
        this->stateManager ().removeActiveState (benzaiten::Fade::None);
    } else {
         StageState *stage (new StageState (
                    this->resources.getFilePath ("stages", *(this->current_stage)),
                    this->resources, this->debug));
        stage->player_escaped.connect (boost::bind (&ListState::next_stage, this));
        this->stateManager ().setActiveState (IGameState::ptr (stage));
    }
}
