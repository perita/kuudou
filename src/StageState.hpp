//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_LD22_STAGE_STATE_HPP)
#define GEISHA_STUDIOS_LD22_STAGE_STATE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <vector>
#include <BitmapFont.hpp>
#include <IGameState.hpp>
#include <Surface.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include "Enemy.hpp"
#include "ExitDoor.hpp"
#include "Label.hpp"
#include "Platform.hpp"
#include "Player.hpp"

namespace benzaiten {
    class ResourceManager;
}

namespace ld22
{

    class StageState: public benzaiten::IGameState {
        public:
            typedef boost::shared_ptr<Enemy> EnemyPtr;
            typedef std::vector<Platform> Platforms;

            mutable boost::signal<void ()> player_escaped;

            StageState (const std::string &file_name,
                    const benzaiten::ResourceManager &resources,
                    bool debug);

            virtual void draw (benzaiten::Surface &screen);
            virtual void update (float elapsed_time);

        private:
            typedef std::vector<EnemyPtr> Enemies;
            typedef std::vector<std::string> Strings;

            void on_action_begin (int action);
            void on_action_end (int action);

            benzaiten::Surface::Color background_color;
            Strings::iterator current_text;
            Enemies enemies;
            ExitDoor exit_door;
            benzaiten::BitmapFont font;
            boost::scoped_ptr<Label> label;
            Platforms platforms;
            Player player;
            Strings strings;

            DECLARE_BENZAITEN_EVENT_TABLE ();
    };

}

#endif // !GEISHA_STUDIOS_LD22_STAGE_STATE_HPP

