//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_LD22_LABEL_HPP)
#define GEISHA_STUDIOS_LD22_LABEL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <Surface.hpp>

namespace benzaiten
{
    class BitmapFont;
}

namespace ld22
{
    class Label {
        public:
            Label (int16_t y, const std::string &text, benzaiten::BitmapFont &font);

            void draw (benzaiten::Surface &screen);
            bool update (float elapsed_time);

        private:
            uint8_t fade_in;
            uint8_t fade_out;
            benzaiten::Surface label;
            float read_time;
            int16_t y;
    };
}

#endif // !GEISHA_STUDIOS_LD22_LABEL_HPP

