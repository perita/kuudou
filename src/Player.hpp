//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_LD22_PLAYER_HPP)
#define GEISHA_STUDIOS_LD22_PLAYER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <Sprite.hpp>

namespace benzaiten {
    class ResourceManager;
}

namespace ld22
{
    class Player
    {
        public:
            enum HPOI {
                LEFT_HAND,
                RIGHT_HAND,
                CHEST
            };

            enum VPOI {
                FEET,
                HEAD,
                SCROTCH
            };

            Player (const benzaiten::ResourceManager &resources);

            void begin_move_left ();
            void begin_move_right ();
            void end_move_left ();
            void end_move_right ();
            void draw_character (benzaiten::Surface &screen);
            void draw_halo (benzaiten::Surface &screen);
            bool is_on_floor () const;
            void jump ();
            void set_on_floor (bool on_floor);
            void set_x (int y, HPOI center_on = CHEST);
            void set_y (int y, VPOI center_on = SCROTCH);
            void stop_jump ();
            void update (float elapsed_time, bool fall_only);

            SDL_Rect get_rect () const {
                SDL_Rect rect = {
                    Sint16((this->x + 0.5) - this->width / 2),
                    Sint16((this->y + 0.5) - this->height / 2),
                    Uint16(this->width), Uint16(this->height) };
                return rect;
            }

        private:
            enum Direction {
                LEFT = -1,
                NONE = 0,
                RIGHT = 1
            };

            Direction direction_x;
            double direction_y;
            bool on_floor;
            benzaiten::Surface spotlight;
            int spotlight_offset_x;
            int spotlight_offset_y;
            benzaiten::Sprite sprite;
            int sprite_offset_x;
            int sprite_offset_y;
            int width;
            int height;
            double x;
            double y;
    };
}

#endif // !GEISHA_STUDIOS_LD22_PLAYER_HPP
