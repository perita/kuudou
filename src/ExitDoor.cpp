//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "ExitDoor.hpp"
#include <ResourceManager.hpp>

using namespace ld22;

ExitDoor::ExitDoor (const benzaiten::ResourceManager &resources):
    surface (resources.graphic ("exit.tga")),
    rect ()
{
    rect.x = 0;
    rect.y = 0;
    rect.w = this->surface.width () / SCREEN_SCALE;
    rect.h = this->surface.height () / SCREEN_SCALE;
}

void ExitDoor::draw (benzaiten::Surface &screen) {
    this->surface.blit (this->rect.x * SCREEN_SCALE, this->rect.y * SCREEN_SCALE, screen);
}

void ExitDoor::move (int16_t x, int16_t y) {
    this->rect.x = x - this->rect.w / 2;
    this->rect.y = y - this->rect.h;
}
