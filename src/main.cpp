//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <SDL_main.h>
#include <boost/scoped_ptr.hpp>
// Benzaiten
#include <Audio.hpp>
#include <EventManager.hpp>
#include <Game.hpp>
#include <GameStateManager.hpp>
#include <ResourceManager.hpp>
#include <System.hpp>
// Game
#include "Actions.hpp"
#include "TitleState.hpp"
#include "StageState.hpp"

using namespace benzaiten;
using namespace ld22;

void show_help (std::ostream &out) {
    out << "ludum dare 22 entry; theme: alone\n";
    out << "\n";
    out << "  -a, --no-audio  Start the game without audio\n";
    out << "  -d, --debug     Start the game in debug mode\n";
    out << "  -h, --help      Show this help screen\n";
    out << "\n";
    out << "This game is ditributed under the terms of the GPL version 2 or any later\n";
    out << "version.\n";
}

int
main (int argc, char *argv[])
{
    try {
        bool debug = false;
        bool use_audio = true;

        int param_index = 1;
        for ( ; param_index < argc; ++param_index) {
            std::string parameter (argv[param_index]);
            if (parameter == "--help" || parameter == "-h") {
                show_help (std::cout);
                return EXIT_SUCCESS;
            } else if (parameter == "--debug" || parameter == "-d") {
                debug = true;
            } else if (parameter == "--no-audio" || parameter == "-a") {
                use_audio = false;
            } else {
                // Assume that is a file name and stop parsing arguments.
                break;
            }
        }

        System system;
        ResourceManager resources ("kuudou");
        system.setIcon (resources, "icon.tga");
        Surface screen (system.setVideoMode (
                    SCREEN_WIDTH * SCREEN_SCALE,
                    SCREEN_HEIGHT * SCREEN_SCALE));
        system.setTitle ("Kuudou");
        boost::scoped_ptr<Audio> audio;
        if (use_audio) {
            audio.reset (new Audio (44100, Audio::S16SYS, Audio::Stereo, 1024));
        }

#if SCREEN_SCALE > 1
        resources.setGraphicScaleFunction (
                boost::bind (benzaiten::fastScale, _1, SCREEN_SCALE));
#endif

        GameStateManager game_state_manager;
        if (param_index < argc) {
            game_state_manager.setActiveState (IGameState::ptr (
                        new StageState (argv[param_index], resources, debug)));
        } else {
new TitleState (resources, debug);
            game_state_manager.setActiveState (
                    IGameState::ptr (new TitleState (resources, debug)),
                    benzaiten::Fade::None);
        }

        EventManager event_manager;
        event_manager.mapKey (SDLK_q, Exit);
        event_manager.mapKey (SDLK_ESCAPE, Exit);
        event_manager.mapKey (SDLK_UP, Jump);
        event_manager.mapKey (SDLK_SPACE, Jump);
        event_manager.mapKey (SDLK_k, Jump);
        event_manager.mapKey (SDLK_LEFT, MoveLeft);
        event_manager.mapKey (SDLK_h, MoveLeft);
        event_manager.mapKey (SDLK_RIGHT, MoveRight);
        event_manager.mapKey (SDLK_l, MoveRight);

        for (int button = 0 ; button < 6; ++button) {
            event_manager.mapJoy (button, Jump);
        }
        event_manager.mapJoy (7, Jump);
        event_manager.mapJoy (6, Exit);
        event_manager.mapJoyAxis (AxisLeft, MoveLeft);
        event_manager.mapJoyAxis (AxisRight, MoveRight);

        // The main has the global music, so it will play all the time.
        benzaiten::Music::ptr bgm (resources.try_music ("bgm.ogg"));
        bgm->play ();

        Game game (event_manager, game_state_manager, screen);
        game.run ();

        return EXIT_SUCCESS;
    } catch (std::exception &e) {
        std::cerr << "Error: " << e.what () << std::endl;
    } catch (...) {
        std::cerr << "Unknown error" << std::endl;
    }
    return EXIT_FAILURE;
}
