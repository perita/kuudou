//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_LD22_ENEMY_HPP)
#define GEISHA_STUDIOS_LD22_ENEMY_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <Sound.hpp>
#include <Sprite.hpp>

namespace benzaiten
{
    class ResourceManager;
}

namespace ld22
{
    class Player;

    class Enemy {
        public:
            Enemy (int16_t from_x, int16_t from_y, int16_t to_x, int16_t to_y,
                    double speed, const benzaiten::Sprite &sprite);
            virtual ~Enemy () { }

            void draw (benzaiten::Surface surface);
            virtual void update (float elapsed_time, const Player &player);
            virtual void reverse_hdir ();
            virtual void reverse_vdir ();

        protected:
            enum HorizontalDirection {
                LEFT = -1,
                HNONE = 0,
                RIGHT = 1
            };

            enum VerticalDirection {
                UP = -1,
                VNONE = 0,
                DOWN = 1
            };

            void update_x (float elapsed_time);
            void update_y (float elapsed_time);

            int16_t from_x;
            int16_t from_y;
            HorizontalDirection hdir;
            benzaiten::Sprite sprite;
            int16_t to_x;
            int16_t to_y;
            double speed;
            VerticalDirection vdir;
            double x;
            double y;
    };

    class Bat: public Enemy {
        public:
            Bat (int16_t from_x, int16_t from_y, int16_t to_x, int16_t to_y,
                    double speed, const benzaiten::ResourceManager &resources);

            static benzaiten::Sprite make_sprite (
                    const benzaiten::ResourceManager &resources);
    };

    class Firefly: public Enemy {
        public:
            Firefly (int16_t x, int16_t y, double speed,
                    const benzaiten::ResourceManager &resources);

            static benzaiten::Sprite make_sprite (
                    const benzaiten::ResourceManager &resources);
            virtual void update (float elapsed_time, const Player &player);

        private:
            bool flee;
            benzaiten::Sound::ptr flee_sound;
    };

    class Turtle: public Enemy {
        public:
            Turtle (int16_t from_x, int16_t to_x, int16_t x, int16_t y, double speed,
                    const benzaiten::ResourceManager &resources);

            static benzaiten::Sprite make_sprite (
                    const benzaiten::ResourceManager &resources);
            virtual void update (float elapsed_time, const Player &player);
            virtual void reverse_hdir ();

        private:
            int pause;
            int pause_timer;
    };
}

#endif // !GEISHA_STUDIOS_LD22_ENEMY_HPP
