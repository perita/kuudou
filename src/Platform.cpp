//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Platform.hpp"
#include <Animation.hpp>

using namespace ld22;

namespace
{
    benzaiten::Surface make_platform (benzaiten::Animation blocks,
            unsigned int width, unsigned int height) {

        benzaiten::Surface platform (width * blocks.width (), height * blocks.height (),
                benzaiten::Surface::screen ());
        platform.fill (0, 0, 0, 0);

        /* Draw the “middle part”. */
        blocks.firstFrame ();
        for (int16_t y = 1, draw_y = blocks.height (); y < height - 1;
                ++y, draw_y += blocks.height ()) {
            for (int16_t x = 0, draw_x = blocks.width () ; x < width - 1;
                    ++x, draw_x += blocks.width ()) {
                blocks.draw (draw_x, draw_y, platform);
            }
        }

        /* Now the middle section of all borders. */
        int16_t end_x = blocks.width () * (width - 1);
        int16_t end_y = blocks.height () * (height - 1);
        for (int16_t y = 1, draw_y = blocks.height (); y < height - 1;
                ++y, draw_y += blocks.height ()) {
            blocks.draw (0, draw_y, platform);
            blocks.draw (end_x, draw_y, platform);
        }
        for (int16_t x = 1, draw_x = blocks.width (); x < width - 1;
                ++x, draw_x += blocks.width ()) {
            blocks.draw (draw_x, 0, platform);
            blocks.draw (draw_x, end_y, platform);
        }

        /* And the corners in order: top-left, bottom-left, top-right, bottom-right. */
        blocks.nextFrame ();
        blocks.draw (0, 0, platform);
        blocks.nextFrame ();
        blocks.draw (0, end_y, platform);
        blocks.nextFrame ();
        blocks.draw (end_x, 0, platform);
        blocks.nextFrame ();
        blocks.draw (end_x, end_y, platform);

        return platform;
    }
}

Platform::Platform (const benzaiten::Animation &animation, int16_t x, int16_t y,
        unsigned int width, unsigned int height):
    height(height),
    surface (make_platform (animation, width, height)),
    width(width),
    x(x),
    y(y)
{
}

void
Platform::draw (benzaiten::Surface &screen) {
    this->surface.blit (this->x * SCREEN_SCALE, this->y * SCREEN_SCALE, screen);
}
