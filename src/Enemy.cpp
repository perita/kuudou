//
// Ludum Dare 22
// Copyright (C) 2011 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Enemy.hpp"
#include <algorithm>
#include <ResourceManager.hpp>
#include "Player.hpp"

using namespace ld22;

Enemy::Enemy (int16_t from_x, int16_t from_y, int16_t to_x, int16_t to_y,
        double speed, const benzaiten::Sprite &sprite):
    from_x (from_x > to_x ? to_x : from_x),
    from_y (from_y > to_y ? to_y : from_y),
    hdir (from_x == to_x ? HNONE : RIGHT),
    sprite (sprite),
    to_x (from_x > to_x ? from_x : to_x),
    to_y (from_y > to_y ? from_y : to_y),
    speed (speed),
    vdir (from_y == to_y ? VNONE : DOWN),
    x (this->from_x),
    y (this->from_y)
{
}

void Enemy::draw (benzaiten::Surface screen) {
    this->sprite.draw (this->x * SCREEN_SCALE - this->sprite.width () / 2,
            this->y * SCREEN_SCALE - this->sprite.height () / 2, screen);
}

void Enemy::update (float elapsed_time, const Player &player) {
    this->update_x (elapsed_time);
    this->update_y (elapsed_time);
}

void Enemy::update_x (float elapsed_time) {
    this->sprite.nextFrame ();
    this->x += this->hdir * this->speed;

    if (this->x > to_x || this->x < from_x) {
        this->reverse_hdir ();
    }
}

void Enemy::update_y (float elapsed_time) {
    this->y += this->vdir * this->speed;
    if (this->y > to_y || this->y < from_y) {
        this->reverse_vdir ();
    }
}

void Enemy::reverse_hdir () {
    this->hdir = HorizontalDirection (-this->hdir);
    // 0 => right animation; 1 => left
    this->sprite.setAnimation (1 - (this->hdir + 1) / 2);
}

void Enemy::reverse_vdir () {
    this->vdir = VerticalDirection (-this->vdir);
}


// --- Bat
Bat::Bat (int16_t from_x, int16_t from_y, int16_t to_x, int16_t to_y,
        double speed, const benzaiten::ResourceManager &resources):
    Enemy (from_x, from_y, to_x, to_y, speed, this->make_sprite (resources))
{
}

benzaiten::Sprite Bat::make_sprite (const benzaiten::ResourceManager &resources) {
    benzaiten::Surface image (resources.graphic ("bat.tga"));
    benzaiten::SpriteSheet sheet (image, image.width ());
    benzaiten::Sprite::Animations animations;
    animations.push_back (benzaiten::Animation (sheet, 0, 2, 5));
    animations.push_back (benzaiten::Animation (sheet, 0, 2, 5));
    return benzaiten::Sprite (animations);
}

// --- Firefly
Firefly::Firefly (int16_t x, int16_t y, double speed,
        const benzaiten::ResourceManager &resources):
    Enemy (x - 5, y - 3, x + 5, y + 3, speed, this->make_sprite (resources)),
    flee (false),
    flee_sound (resources.try_sound ("firefly.wav"))
{
}

benzaiten::Sprite Firefly::make_sprite (const benzaiten::ResourceManager &resources) {
    benzaiten::Surface image (resources.graphic ("firefly.tga"));
    benzaiten::SpriteSheet sheet (image, image.width ());
    benzaiten::Sprite::Animations animations;
    animations.push_back (benzaiten::Animation (sheet, 0, 1));
    animations.push_back (benzaiten::Animation (sheet, 0, 1));
    return benzaiten::Sprite (animations);
}

void Firefly::update (float elapsed_time, const Player &player) {
    if (this->flee) {
        this->y -= this->speed * 2;
        this->update_x (elapsed_time);
    } else {
        Enemy::update (elapsed_time, player);
        SDL_Rect rect = player.get_rect ();
        int dir_x = this->x - rect.x;
        int dir_y = this->y - rect.y;
        if (dir_x * dir_x + dir_y * dir_y < 20 * 20) {
            this->flee = true;
            this->flee_sound->play ();
        }
    }
}

Turtle::Turtle (int16_t from_x, int16_t to_x, int16_t x, int16_t y, double speed,
        const benzaiten::ResourceManager &resources):
    Enemy (from_x + 4, y - 2, to_x - 4, y, speed, this->make_sprite (resources)),
    pause (30),
    pause_timer (0)
{
    this->x = std::max (this->from_x, std::min (x, this->to_x));
}

benzaiten::Sprite Turtle::make_sprite (const benzaiten::ResourceManager &resources) {
    using namespace benzaiten;
    Surface image (resources.graphic ("turtle.tga"));
    SpriteSheet right_sheet (image, image.width ());
    SpriteSheet left_sheet (flip (image, true, false), image.width ());
    Sprite::Animations animations;
    animations.push_back (Animation (right_sheet, 0, 2, 7));
    animations.push_back (Animation (left_sheet, 0, 2, 7));
    return Sprite (animations);
}

void Turtle::update (float elapsed_time, const Player &player) {
    if (this->pause_timer > 0) {
        --this->pause_timer;
        if (this->pause_timer == 0) {
            Enemy::reverse_hdir ();
        }
    } else {
        this->update_x (elapsed_time);
    }
}

void Turtle::reverse_hdir () {
    this->pause_timer = this->pause;
}
